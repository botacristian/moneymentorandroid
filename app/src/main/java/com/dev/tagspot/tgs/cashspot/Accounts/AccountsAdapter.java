package com.dev.tagspot.tgs.cashspot.Accounts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristi on 6/15/2017.
 */

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.CustomViewHolder> {

    private List<Account> accountList;
    private OnAccountsButtonClicked onAccountsButtonClicked;

    public AccountsAdapter(List<Account> accounts, OnAccountsButtonClicked onAccountsButtonClicked)  {

        if(accountList == null){
            accountList = new ArrayList<>();
        }
        this.accountList = accounts;
        this.onAccountsButtonClicked = onAccountsButtonClicked;

    }

    private void addAccount(Account account){
        if(accountList == null){
            accountList = new ArrayList<>();
        }
        accountList.add(account);
        notifyDataSetChanged();
    }

    public AccountsAdapter() {
        if(accountList == null){
            accountList = new ArrayList<>();
        }
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_list_item, parent, false);
        return new CustomViewHolder(item) ;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final Account account = accountList.get(position);
        holder.title.setText(account.getName());
        holder.amount.setText(String.valueOf(account.getSum()));
        holder.editAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAccountsButtonClicked.onEditClicked(account);
            }
        });
        holder.deleteAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onAccountsButtonClicked.onDeleteClicked(account);
            }
        });
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }

    static class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView amount, title;
         Button editAcc, deleteAcc;

        CustomViewHolder(View itemView) {
            super(itemView);
            amount = (TextView) itemView.findViewById(R.id.amount);
            title = (TextView) itemView.findViewById(R.id.name);
            editAcc = (Button) itemView.findViewById(R.id.edit_btn);
            deleteAcc = (Button) itemView.findViewById(R.id.delete_btn);
        }
    }

    public interface OnAccountsButtonClicked{
        void onDeleteClicked(Account account);
        void onEditClicked(Account account);
    }
}
