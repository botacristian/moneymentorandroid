package com.dev.tagspot.tgs.cashspot;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dev.tagspot.tgs.cashspot.Accounts.AccountsAdapter;
import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

/**
 * Created by Tgs on 3/31/2017.
 */

public class AccountsFragment extends Fragment implements View.OnClickListener, AccountsAdapter.OnAccountsButtonClicked,  CustomSuccessCallback {


    private static final String TAG = AccountsFragment.class.getSimpleName();
    private static final long SINGLE_SHOT_ID = 182793;
    Account account;
    private Button setAmountBtn;
    RecyclerView accountsRecyclerView;
    LinearLayoutManager linearLayoutManager;
    List<Account> accountList;
    private boolean hasAccounts = true;
    FloatingActionButton addAccountBtn;
    ShowcaseView sv;
    RelativeLayout.LayoutParams lps;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.accounts_fragment, container, false);

//        setAmountBtn.setOnClickListener(this);

        init(v);

        setAccounts(MainActivity.accountList, v);

        return v;
    }


    public void setAccounts(List<Account> accountList, final View v) {
        //TODO
        Log.e(TAG, "setAccounts");
        accountsRecyclerView.setAdapter(new AccountsAdapter(accountList, this));
        if (accountList.size() == 0) {
            final Button button = new Button(getActivity());
            button.setText("");
            button.setVisibility(View.INVISIBLE);

                lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    // This aligns button to the bottom left side of screen
                lps.addRule(RelativeLayout.CENTER_IN_PARENT);
                lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
    // Set margins to the button, we add 16dp margins here
                int margin = ((Number) (getResources().getDisplayMetrics().density * 32)).intValue();
                lps.setMargins(margin, margin, margin, margin);

                Handler h  = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ViewTarget target = new ViewTarget(v.findViewById(R.id.addCategoryBtn));
                        sv = new ShowcaseView.Builder(getActivity())
                                .withMaterialShowcase()
                                .setTarget(target)
                                .singleShot(SINGLE_SHOT_ID)
                                .setContentTitle(getString(R.string.create_account))
                                .setContentText(getString(R.string.infoTextTransactionDialog))
                                .hideOnTouchOutside()
                                .build();
                        // Set declared button position to ShowcaseView
                        sv.setButtonPosition(lps);
                    }
                }, 450);


        }
    }

    private void init(View v) {
        addAccountBtn = (FloatingActionButton) v.findViewById(R.id. addCategoryBtn);
        addAccountBtn.setOnClickListener(this);

        accountsRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        accountsRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        accountsRecyclerView.setLayoutManager(linearLayoutManager);
        Log.d(TAG, "onCreate INIT()");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.next:
//                ((MainActivity) getActivity()).changeFragment(new DashboardFragment());
//                break;
            case R.id.addCategoryBtn:
                showDialogPopup();
                break;
        }
    }

    private void openUpdateAccountWindow(final Account account) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.account_update_dialog);
        EditText name = (EditText) dialog.findViewById(R.id.nameEt);
        final EditText amount = (EditText) dialog.findViewById(R.id.amountEt);
        name.setHint(account.getName());
        amount.setHint(String.valueOf(account.getSum()));

        Button b = (Button) dialog.findViewById(R.id.doneBtn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    EditText nameEt = (EditText) dialog.findViewById(R.id.nameEt);
                    EditText amountEt = (EditText) dialog.findViewById(R.id.amountEt);
                    Log.d(TAG, "nameET= " + nameEt.getText().toString().length());
                    Log.d(TAG, "amountEt= " + amountEt.getText().toString().length());
                    if((nameEt.getText().toString().length() > 0) &&
                            (amountEt.getText().toString().length() > 0)){
                        Log.e(TAG, "condition = true ");
                        final String accName = nameEt.getText().toString();
                        final Float amountStr = Float.valueOf(amountEt.getText().toString());
                        account.updateAccountNameAndSum(accName, amountStr, AccountsFragment.this);
                        dialog.dismiss();

                    }else if (nameEt.getText().toString().length() > 0){
                        final String accName = nameEt.getText().toString();
                        account.updateAccountName(accName, AccountsFragment.this);
                        dialog.dismiss();
                    } else if (amountEt.getText().toString().length() > 0){
                        final Float amountStr = Float.valueOf(amountEt.getText().toString());
                        account.updateAccountSumInDB(amountStr, AccountsFragment.this);
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
                dialog.show();
    }

    private void createAnewAccount(Account account) {
        DatabaseReference ref = MainActivity.userIdReference.child("accounts");
        String id = ref.push().getKey();

        ref.child(id).setValue(account, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(TAG, "Data could not be saved " + databaseError.getMessage());
                } else {
//                    retrieveAccounts();
//                    ((MainActivity) getActivity()).getAndSetAccountList();
                    ((MainActivity) getActivity()).retrieveAccountsAndSetAccountsFragment();

                }
            }
        });
        Log.d(TAG, "account = " + account.toString());
        //TODO ADD LOADING DIALOG


    }

    private void showDialogPopup() {
        Log.d(TAG, "showDialogPopup");
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.transaction_dialog_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView back = (ImageView) dialog.findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button b = (Button) dialog.findViewById(R.id.doneBtn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //TODO Validare pe nume si amount
                    String accName = ((EditText) dialog.findViewById(R.id.nameEt)).getText().toString();
                    String amount = ((EditText) dialog.findViewById(R.id.amountEt)).getText().toString();
                    Float floatValue = Float.valueOf(amount);

                    Account account = new Account(accName, floatValue);
                    createAnewAccount(account);
                    dialog.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }


    @Override
    public void onDeleteClicked(Account account) {
        Log.d(TAG, "delete account name = " + account.getName());
        account.deleteAccount(account, AccountsFragment.this);
    }

    @Override
    public void onEditClicked(Account account) {
        Log.d(TAG, "edit account name = " + account.getName());
        openUpdateAccountWindow(account);

    }

    @Override
    public void onSuccess() {
        ((MainActivity) getActivity()).retrieveAccountsAndSetAccountsFragment();
    }

    @Override
    public void onError() {
        Log.e("ERROR", "error !!");

    }


}
