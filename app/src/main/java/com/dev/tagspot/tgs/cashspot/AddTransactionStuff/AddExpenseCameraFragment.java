package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionAdd;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionSubtract;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

/**
 * Created by Cristi on 5/7/2017.
 */

public class AddExpenseCameraFragment extends AddTransactionAbstractClass implements OnItemClicked, View.OnClickListener {

    private static final String TAG = AddExpenseCameraFragment.class.getSimpleName();

    Uri fileUri;
    MaterialSpinner spinner;
    ImageView image;
    String selectedAccount;
    Uri downloadUrl;
    Button saveBtn;
    String postUID, formattedDate;
    EditText nameEt, priceEt;
    int year, month, day, hour, minute;

    String[] permissions = new String[]{
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    final PermissionCallback permissionCameraCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {

            Toast.makeText(getContext(), "Access granted = true", Toast.LENGTH_SHORT).show();
            getCameraPermission();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };

    final PermissionCallback permissionReadStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {

            Toast.makeText(getContext(), "Access granted = true", Toast.LENGTH_SHORT).show();
            getReadStoragePermission();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };

    final PermissionCallback permissionWriteStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            getWriteStoragePermission();
            Toast.makeText(getContext(), "permissionWriteStorageCallback Access granted = true", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "permissionWriteStorageCallback Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_expense_camera_layout, container, false);


        //TODO set good items
        spinner = (MaterialSpinner) v.findViewById(R.id.spinner);

        image = (ImageView) v.findViewById(R.id.photoImageView);
        image.setOnClickListener(this);
        saveBtn = (Button) v.findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);

        nameEt = (EditText) v.findViewById(R.id.nameEt);
        priceEt = (EditText) v.findViewById(R.id.priceEt);

        try {
            fileUri = Uri.parse(getArguments().getString("uri"));
            if (fileUri != null) {
                Picasso.with(getActivity()).load(fileUri).into(image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            spinner.setItems(getAccountsNames());
            selectedAccount = spinner.getItems().get(0).toString();
            spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    selectedAccount = item;
                }
            });

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Log.d(TAG, "picked image");
                fileUri = Uri.fromFile(new File(String.valueOf(imageFile)));
                Log.d(TAG, "file path = " + imageFile.toString());
//                Picasso.with(getActivity()).load(fileUri).into(image);
//                uploadPhotoToCloud(fileUri);
            }
        });
    }


    public void uploadPhotoToCloud(Uri filePath) {
//        if (nameEt.getText().toString().equals("")) {
//            nameEt.requestFocus();
//            nameEt.setHint(getString(R.string.please_enter_name));
//            return;
//        }
//        if (priceEt.getText().toString().equals("")) {
//            priceEt.requestFocus();
//            priceEt.setHint(getString(R.string.please_enter_price));
//            return;
//        }
        ((MainActivity) getActivity()).showProgressDialog();
        Log.e(TAG, "uploadPhotoToCloud started");
        FirebaseStorage storage = FirebaseStorage.getInstance();

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        postUID = UUID.randomUUID().toString();
        StorageReference reference = storage.getReference().child(userId).child(postUID);
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpg").build();
        UploadTask task = reference.putFile(filePath, metadata);
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                Log.e("ERROR", "ERROR!!");

                ((MainActivity) getActivity()).dismissProgressDialog();
            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                downloadUrl = task.getResult().getDownloadUrl();
                Log.e(TAG, "downloadUrl = " + downloadUrl.toString());
                ((MainActivity) getActivity()).dismissProgressDialog();
                Snackbar.make(spinner, getString(R.string.success_uploading_photo), Snackbar.LENGTH_LONG);
                try {
                    Picasso.with(getActivity()).load(fileUri).fit().into(image);
                    createPostAndUpload();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        Log.e(TAG, "uploadPhotoToCloud ended");

    }

    void createPostAndUpload() {

        Log.d(TAG, "createPostAndUpload");
        setTodayDate();
        if (postUID != null && downloadUrl != null) {
            float value = 0;
            String flag = Transaction.FLAG_COMPLETE;
            try{
                 value = Float.valueOf(priceEt.getText().toString());
            } catch (Exception e){
                e.printStackTrace();
            }
            if(value == 0){
                flag = Transaction.FLAG_INCOMPLETE;
            }

            TransactionSubtract a = new TransactionSubtract.TransactionBuilder()
                    .addName(nameEt.getText().toString())
                    .addValue(value)
                    .addDestinationAccountName(selectedAccount)
                    .addDate(formattedDate)
                    .addHour(hour)
                    .addFlag(flag)
                    .addMinute(minute)
                    .addPhotoURL(downloadUrl)
                    .addPhotoUID(postUID)
                    .build();
            a.executeTransactionSubtract(getActivity());
        } else {
            //todo show user error
            Log.e("ERROR", "createPostAndUpload ERROR");
        }
    }


    void setTodayDate() {
        Calendar c = Calendar.getInstance();
        day = c.get(Calendar.DAY_OF_MONTH);
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        updateDisplay();

    }

    void updateDisplay() {
        GregorianCalendar g = new GregorianCalendar(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //set EditText correct date

        formattedDate = sdf.format(g.getTime());
        Log.d(TAG, "transDateString = " + formattedDate);
        //Console Log purposes

    }

    void customTakePic() {
        EasyImage.openCamera(AddExpenseCameraFragment.this, 0);
    }

    void getWriteStoragePermission() {
        if (Nammu.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            viewSnackBar("Permission allowed", spinner);
            customTakePic();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseCameraFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(getView(), "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
            }
        }
    }


    void getReadStoragePermission() {
        if (Nammu.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            viewSnackBar("Permission allowed", spinner);
            getWriteStoragePermission();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseCameraFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(getView(), "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
            }
        }
    }


    public void getCameraPermission() {
        if (Nammu.checkPermission(Manifest.permission.CAMERA)) {
//            viewSnackBar("Permission allowed", spinner);
            getReadStoragePermission();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseCameraFragment.this, Manifest.permission.CAMERA)) {
                Snackbar.make(getView(), "Grant permission to access the camera",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.CAMERA, permissionCameraCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseCameraFragment.this, Manifest.permission.CAMERA, permissionCameraCallback);
            }
        }
    }

    public static void viewSnackBar(String message, View view) {
        Snackbar snackbar = Snackbar.make(view, message,
                Snackbar.LENGTH_LONG).setDuration(Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setMaxLines(5);
        snackbar.show();
    }


    @Override
    public void onCategoryClicked(Category category) {

        Log.d(TAG, category.getName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photoImageView:
                getCameraPermission();
                break;
            case R.id.saveBtn:
                uploadPost();
        }

    }

    private void uploadPost() {

        uploadPhotoToCloud(fileUri);

    }
}
