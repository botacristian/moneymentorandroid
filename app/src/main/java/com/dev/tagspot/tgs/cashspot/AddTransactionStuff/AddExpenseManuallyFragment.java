package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.Categories.SelectCategoryFragment;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionSubtract;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;


/**
 * Created by Tgs on 5/8/2017.
 */

public class AddExpenseManuallyFragment extends AddTransactionAbstractClass implements View.OnClickListener, SelectCategoryFragment.OnCategorySelected, SelectSubcategoryFragment.OnSubcategorySelected {

    private static final String TAG = AddExpenseManuallyFragment.class.getSimpleName();
    MaterialSpinner spinner;
    TextView categoryTv, dateText;
    ImageView cameraImage, testImage, recursiveIcon;
    EditText priceEt, nameEt;
    String category, subcategory, formattedDate, selectedAccount, postUID;
    private Category selectedCategory;
    Uri fileUri;
    int year, month, day, hour, minute;
    float value = 0;
    Uri downloadUrl;
    static boolean isSubCategoriesListShowing = false;
    String[] permissions = new String[]{
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    protected boolean shouldUpdatePost = false;
    TransactionSubtract t;
    CustomSuccessCallback callback;


//    public static AddExpenseManuallyFragment newInstance(List<Transaction> incompleteTransactions){
//        Bundle args = new Bundle();
//        args.put("transactions", incompleteTransactions);
//    }

    public static AddExpenseManuallyFragment newInstance(Transaction transaction, CustomSuccessCallback callback) {
        AddExpenseManuallyFragment f = new AddExpenseManuallyFragment();
        f.setCallback(callback);
        Bundle b = new Bundle();
        b.putParcelable("transaction", transaction);
        f.setArguments(b);
        return f;
    }

    final PermissionCallback permissionCameraCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            getCameraPermission();
            Toast.makeText(getContext(), "permissionCameraCallback Access granted = true", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "permissionCameraCallback Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };


    final PermissionCallback permissionReadStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            getReadStoragePermission();
            Toast.makeText(getContext(), "permissionReadStorageCallback Access granted = true", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "permissionReadStorageCallback Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };

    final PermissionCallback permissionWriteStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            getWriteStoragePermission();
            Toast.makeText(getContext(), "permissionWriteStorageCallback Access granted = true", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(getContext(), "permissionWriteStorageCallback Access granted = false", Toast.LENGTH_SHORT).show();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_expense_manually_layout, container, false);

        try {
            Bundle b = getArguments();
            t = b.getParcelable("transaction");
            Log.d(TAG, "from Parcelable = " + t.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }


        /* ------- spinner ----------*/
        spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        // --------------- */

        dateText = (TextView) v.findViewById(R.id.dateTv);
        v.findViewById(R.id.addPhotoLayout).setOnClickListener(this);
        v.findViewById(R.id.dateLayout).setOnClickListener(this);
        v.findViewById(R.id.saveBtn).setOnClickListener(this);

        categoryTv = (TextView) v.findViewById(R.id.categoryTv);
        categoryTv.setOnClickListener(this);
        v.findViewById(R.id.categoryLayout).setOnClickListener(this);

        isSubCategoriesListShowing = false;

        priceEt = (EditText) v.findViewById(R.id.priceEt);
        nameEt = (EditText) v.findViewById(R.id.nameEt);


        recursiveIcon = (ImageView) v.findViewById(R.id.recursive_icon);
        recursiveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WheelView wheelView = (WheelView) v.findViewById(R.id.loop_view);


            }
        });

        cameraImage = (ImageView) v.findViewById(R.id.cameraImage);
        testImage = (ImageView) v.findViewById(R.id.testImage);


        setTodayDate();
        if (t != null) {

            shouldUpdatePost = true;
            setDataFromTransaction();
            Log.d(TAG, "t != null");

        } else {

            Log.d(TAG, "t == null");
        }


        return v;

    }

    public void setCallback(CustomSuccessCallback callback) {
        this.callback = callback;
    }

    protected void setDataFromTransaction() {

        //TODO Set correct account

        try {

            Log.d(TAG, "value = " + t.getValue());
            priceEt.setText(String.valueOf(t.getValue()));

            if (t.getName() != null && !t.getName().equals("")) {
                nameEt.setText(t.getName());
            }

            dateText.setText(t.getDate());
            if (t.getPhotoURL() != null && !t.getPhotoURL().equals(""))
                Picasso.with(getActivity()).load(t.getPhotoURL()).into(cameraImage);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //TODO fill in fields with transaction data


    }


    /**
     * View Snack bar for simple form
     *
     * @param message the message to shown
     * @param view    the principal view (layout)
     */
    public static void viewSnackBar(String message, View view) {
        Snackbar snackbar = Snackbar.make(view, message,
                Snackbar.LENGTH_LONG).setDuration(Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setMaxLines(5);
        snackbar.show();
    }

    protected void setTodayDate() {
        Calendar c = Calendar.getInstance();
        day = c.get(Calendar.DAY_OF_MONTH);
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        updateDisplay();

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            spinner.setItems(getAccountsNames());
            selectedAccount = spinner.getItems().get(0).toString();
            spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    selectedAccount = item;
                }
            });
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    void getCameraPermission() {
        if (Nammu.checkPermission(Manifest.permission.CAMERA)) {
            viewSnackBar("Permission allowed", categoryTv);
            getReadStoragePermission();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseManuallyFragment.this, Manifest.permission.CAMERA)) {
                Snackbar.make(getView(), "Grant permission to access the camera",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.CAMERA, permissionCameraCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.CAMERA, permissionCameraCallback);
            }
        }
    }

    void getReadStoragePermission() {
        if (Nammu.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            viewSnackBar("Permission allowed", categoryTv);
            getWriteStoragePermission();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseManuallyFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(getView(), "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
            }
        }
    }

    void getWriteStoragePermission() {
        if (Nammu.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            viewSnackBar("Permission allowed", categoryTv);
            customTakePic();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(AddExpenseManuallyFragment.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(getView(), "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(AddExpenseManuallyFragment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    void customTakePic() {
        EasyImage.openCamera(AddExpenseManuallyFragment.this, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Log.d(TAG, "picked image");
                fileUri = Uri.fromFile(new File(String.valueOf(imageFile)));
                Picasso.with(getActivity()).load(fileUri).into(cameraImage);
                Log.d(TAG, "file path = " + imageFile.toString());

            }
        });
    }

    public void uploadPhotoToCloud(Uri filePath) {
        //TODO When updating the post and adding a new picture, search for the last one and delete it
        final ProgressDialog d = new ProgressDialog(getActivity());
        d.setMessage(getString(R.string.loading));
        d.show();
        Log.e(TAG, "uploadPhotoToCloud started");
        FirebaseStorage storage = FirebaseStorage.getInstance();

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        postUID = UUID.randomUUID().toString();
        StorageReference reference = storage.getReference().child(userId).child(postUID);
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpg").build();
        UploadTask task = reference.putFile(filePath, metadata);
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                d.dismiss();
                downloadUrl = null;
            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                downloadUrl = task.getResult().getDownloadUrl();
                Log.e(TAG, "downloadUrl = " + downloadUrl.toString());
                d.dismiss();
                Snackbar.make(spinner, getString(R.string.success_uploading_photo), Snackbar.LENGTH_LONG);
                try {
                    Picasso.with(getActivity()).load(fileUri).fit().into(cameraImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!shouldUpdatePost) {
                    takeInput();
                }
            }
        });

        Log.e(TAG, "uploadPhotoToCloud ended");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.categoryLayout:
            case R.id.categoryTv:
                Log.d(TAG, "clicked");
                showSelectCategoryFragment(this);
                break;
            case R.id.addPhotoLayout:
                takePic();
                break;
            case R.id.dateLayout:
                showDatePicker();
                break;
            case R.id.saveBtn:
                uploadPost();
                break;
        }

    }

    void uploadPost() {
        if (isValidAmount(priceEt)) {
            if (fileUri != null) {
                uploadPhotoToCloud(fileUri);
            } else {
                updatePost();
            }
        }
    }

    void updateDisplay() {
        GregorianCalendar g = new GregorianCalendar(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //set EditText correct date

        formattedDate = sdf.format(g.getTime());
        Log.d(TAG, "transDateString = " + formattedDate);
        //Console Log purposes


        dateText.setText(formattedDate);
    }

    void showDatePicker() {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                AddExpenseManuallyFragment.this.year = year;
                AddExpenseManuallyFragment.this.month = month;
                day = dayOfMonth;
                updateDisplay();
            }
        };

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), dateSetListener, year, month, day);
        Calendar c = Calendar.getInstance();
        datePickerDialog.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    void updatePost() {
        if (shouldUpdatePost) {
            updateTransaction();
        } else {
            takeInput();
        }
    }

    private void updateTransaction() {

        // TODO ce se intampla daca updateaza poza?

        t.setValue(value);
        if (!nameEt.getText().toString().equals("")) {
            t.setName(nameEt.getText().toString());
        }
        t.setDestinationAccountName(selectedAccount);
        if (category != null) {
            t.setCategory(category);
            if (subcategory != null) {
                t.setSubcategory(subcategory);
            }
        }
        t.setCompletionFlag(Transaction.FLAG_COMPLETE);

        t.updateTransactionSubtract(getActivity(), t, callback);

    }


    void takeInput() {
        try {
            TransactionSubtract a = new TransactionSubtract.TransactionBuilder().
                    addCategory(category)
                    .addSubcategory(subcategory)
                    .addDate(formattedDate)
                    .addHour(hour)
                    .addMinute(minute)
                    .addDestinationAccountName(selectedAccount)
                    .addPhotoURL(downloadUrl)
                    .addPhotoUID(postUID)
                    .addName(getName())
                    .addValue(value)
                    .build();
            Log.e(TAG, "a = " + a.toString());
            a.executeTransactionSubtract(getActivity(), a);

            if (category != null && !category.equals("")) {
                updateCategory(selectedCategory, value, TransactionSubtract.SUBTRACT);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(priceEt, "Error. Please check data", Snackbar.LENGTH_LONG).show();
        }


    }

    void updateCategory(Category selectedCategory, float value, String type) {
        Log.d(TAG, "value = " + value);
        Log.d(TAG, "type = " + type);
        selectedCategory.updateCategory(value, type);
    }

    String getName() {
        return nameEt.getText().toString().length() > 0 ? nameEt.getText().toString() : "";
    }

    boolean isValidAmount(EditText priceEt) {
        //// TODO: 7/11/2017

        if (priceEt.getText().toString().length() > 0) {
            priceEt.setHintTextColor(Color.DKGRAY);
            String amountString = priceEt.getText().toString();
            value = Float.valueOf(amountString);
            return true;
        } else {

            priceEt.setHintTextColor(Color.RED);
            return false;
        }
    }

    void takePic() {

        getCameraPermission();


//        if(isCameraPermissionGranted() && isStoragePermissionGranted()){
//
//            EasyImage.openCamera(AddExpenseManuallyFragment.this, 0);
//        }


//        if (Global.isDeviceSupportCamera() && isStoragePermissionGranted() && isCameraPermissionGranted()) {
//
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            fileUri = ImageHelper.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, getActivity());
//
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Permission not granted or your device doesn't support camera",
//                    Toast.LENGTH_LONG).show();
//        }
    }

    @Override
    public void onCategorySelected(Category c) {
        Log.e(TAG, "onCategorySelected = " + c.getName());
        selectedCategory = c;
        category = c.getName();
        categoryTv.setText(c.getName() + "");
        c.getSubcategoriesAsync(getActivity(), new Category.OnSubcategoriesRetrievedListener() {
            @Override
            public void onSubcategoriesRetrieved(List<Category> subcategories) {
                if (subcategories == null) {
                    Log.e(TAG, "onCategorySelected = subcategories == null");
                    // todo does not have subcategories
                } else {
                    if (!isSubCategoriesListShowing) {
                        showSelectSubcategoryFragment(AddExpenseManuallyFragment.this, subcategories);
                        isSubCategoriesListShowing = true;
                        Log.e(TAG, "onCategorySelected = subcategories != null");
                        // todo has subcategories
                    }
                }
            }
        });
    }

    @Override
    public void onSubcategorySelected(Category c) {
        Log.d(TAG, "c name = " + c);
        categoryTv.append(" " + c.getName());

        subcategory = c.getName();
    }


}
