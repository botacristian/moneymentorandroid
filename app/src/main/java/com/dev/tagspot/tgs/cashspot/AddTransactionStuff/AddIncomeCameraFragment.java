package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;

import android.util.Log;

import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionAdd;

/**
 * Created by Tgs on 8/31/2017.
 */

public class AddIncomeCameraFragment extends AddExpenseCameraFragment {

    private static final String TAG = AddIncomeCameraFragment.class.getSimpleName();

    @Override
    void createPostAndUpload() {
        Log.d(TAG, "createPostAndUpload");


        setTodayDate();
        if (postUID != null && downloadUrl != null) {
            float value = 0;
            String flag = Transaction.FLAG_COMPLETE;
            try{
                value = Float.valueOf(priceEt.getText().toString());
            } catch (Exception e){
                e.printStackTrace();
            }
            if(value == 0){
                flag = Transaction.FLAG_INCOMPLETE;
            }

            TransactionAdd a = new TransactionAdd.TransactionBuilder()
                    .addName(nameEt.getText().toString())
                    .addValue(value)
                    .addDestinationAccountName(selectedAccount)
                    .addDate(formattedDate)
                    .addHour(hour)
                    .addFlag(flag)
                    .addMinute(minute)
                    .addPhotoURL(downloadUrl)
                    .addPhotoUID(postUID)
                    .build();
            a.executeTransactionAdd(getActivity());
        } else {
            //todo show user error
            Log.e("ERROR", "createPostAndUpload ERROR");
        }
    }
}
