package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionAdd;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionSubtract;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Tgs on 8/31/2017.
 */

public class AddIncomeManuallyFragment extends AddExpenseManuallyFragment {

    private static final String TAG = AddIncomeManuallyFragment.class.getSimpleName();
    Category selectedCategory;

    TransactionAdd t;

    public static AddIncomeManuallyFragment newInstance(Transaction transaction, CustomSuccessCallback callback) {
        AddIncomeManuallyFragment f = new AddIncomeManuallyFragment();
        f.setCallback(callback);
        Bundle b = new Bundle();
        b.putParcelable("transaction", transaction);
        f.setArguments(b);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_expense_manually_layout, container, false);

        try {
            Bundle b = getArguments();
            this.t = b.getParcelable("transaction");
            Log.d(TAG, "from Parcelable = " + this.t.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }


        /* ------- spinner ----------*/
        spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        // --------------- */

        dateText = (TextView) v.findViewById(R.id.dateTv);
        v.findViewById(R.id.addPhotoLayout).setOnClickListener(this);
        v.findViewById(R.id.dateLayout).setOnClickListener(this);
        v.findViewById(R.id.saveBtn).setOnClickListener(this);

        categoryTv = (TextView) v.findViewById(R.id.categoryTv);
        categoryTv.setOnClickListener(this);
        v.findViewById(R.id.categoryLayout).setOnClickListener(this);

        isSubCategoriesListShowing = false;

        priceEt = (EditText) v.findViewById(R.id.priceEt);
        nameEt = (EditText) v.findViewById(R.id.nameEt);


        recursiveIcon = (ImageView) v.findViewById(R.id.recursive_icon);
        recursiveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WheelView wheelView = (WheelView) v.findViewById(R.id.loop_view);


            }
        });

        cameraImage = (ImageView) v.findViewById(R.id.cameraImage);
        testImage = (ImageView) v.findViewById(R.id.testImage);



        setTodayDate();
        if (t != null) {

            shouldUpdatePost = true;
            setDataFromTransaction();
            Log.d(TAG, "t != null");

        } else {

            Log.d(TAG, "t == null");
        }


        return v;

    }


    @Override
    protected void setDataFromTransaction() {

        if (t != null) {
            Log.d(TAG, "t != null");
        } else {

            Log.d(TAG, "t == null");
        }
        //TODO Set correct account

        try {

            Log.d(TAG, "value = " + t.getValue());
            priceEt.setText(String.valueOf(t.getValue()));

            if (t.getName() != null && !t.getName().equals("")) {
                nameEt.setText(t.getName());
            }

            dateText.setText(t.getDate());
            if (t.getPhotoURL() != null && !t.getPhotoURL().equals(""))
                Picasso.with(getActivity()).load(t.getPhotoURL()).into(cameraImage);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //TODO fill in fields with transaction data


    }

    @Override
    void uploadPost() {
        if (isValidAmount(priceEt)) {
            if (fileUri != null) {
                uploadPhotoToCloud(fileUri);
            } else {
                updatePost();
            }
        }
    }


    void updatePost() {
        if (shouldUpdatePost) {
            updateTransaction();
        } else {
            takeInput();
        }
    }


    private void updateTransaction() {

        // TODO ce se intampla daca updateaza poza?

        float value = Float.valueOf(priceEt.getText().toString());
        t.setValue(value);
        if (!nameEt.getText().toString().equals("")) {
            t.setName(nameEt.getText().toString());
        }
        t.setDestinationAccountName(selectedAccount);
        if (category != null) {
            t.setCategory(category);
            if (subcategory != null) {
                t.setSubcategory(subcategory);
            }
        }
        t.setCompletionFlag(Transaction.FLAG_COMPLETE);

        t.updateTransactionAdd(getActivity(), t, callback);

    }


    @Override
    void takeInput() {


            try {
                TransactionAdd a = new TransactionAdd.TransactionBuilder().
                        addCategory(category)
                        .addSubcategory(subcategory)
                        .addDate(formattedDate)
                        .addHour(hour)
                        .addMinute(minute)
                        .addDestinationAccountName(selectedAccount)
                        .addPhotoURL(downloadUrl)
                        .addPhotoUID(postUID)
                        .addName(getName())
                        .addValue(value)
                        .build();
                Log.e(TAG, "a = " + a.toString());
                a.executeTransactionAdd(getActivity(), a);


                if(category != null && !category.equals("")){
                    updateCategory(selectedCategory, value, TransactionSubtract.ADD);
                }
        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(priceEt, "Error. Please check data", Snackbar.LENGTH_LONG).show();
        }

    }


    @Override
    public void onCategorySelected(Category c) {
        Log.e(TAG, "onCategorySelected = " + c.getName());
        selectedCategory = c;
        category = c.getName();
        categoryTv.setText(c.getName() + "");
        c.getSubcategoriesAsync(getActivity(), new Category.OnSubcategoriesRetrievedListener() {
            @Override
            public void onSubcategoriesRetrieved(List<Category> subcategories) {
                if (subcategories == null) {
                    Log.e(TAG, "onCategorySelected = subcategories == null");
                    // todo does not have subcategories
                } else {
                    if (!isSubCategoriesListShowing) {
                        showSelectSubcategoryFragment(AddIncomeManuallyFragment.this, subcategories);
                        isSubCategoriesListShowing = true;
                        Log.e(TAG, "onCategorySelected = subcategories != null");
                        // todo has subcategories
                    }
                }
            }
        });
    }
}
