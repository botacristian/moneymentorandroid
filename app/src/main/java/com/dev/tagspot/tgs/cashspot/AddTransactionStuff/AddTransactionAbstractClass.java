package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.dev.tagspot.tgs.cashspot.Categories.SelectCategoryFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.POJO.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristi on 6/26/2017.
 */

public abstract class AddTransactionAbstractClass extends Fragment {
    private static final String TAG = AddTransactionAbstractClass.class.getSimpleName();
    SelectCategoryFragment dialogFragment;

    public List<String> getAccountsNames() throws IndexOutOfBoundsException {
        List<String> strings = new ArrayList<>();
        if (MainActivity.accountList != null && MainActivity.accountList.size() > 0) {
            for (int i = 0; i < MainActivity.accountList.size(); i++) {
                strings.add(MainActivity.accountList.get(i).getName());
            }
        } else {
            //TODO
        }
        return strings;
    }

    public void showSelectCategoryFragment(SelectCategoryFragment.OnCategorySelected onCategorySelectedListener) {
        if (dialogFragment == null) {
            dialogFragment = SelectCategoryFragment.newInstanceForSelectCategory(onCategorySelectedListener);
            showCustomFragment(dialogFragment);
            Log.d(TAG, "new DialogFragment");
        } else {
            showCustomFragment(dialogFragment);
        }
    }

    public void showSelectSubcategoryFragment(SelectSubcategoryFragment.OnSubcategorySelected onSubcategorySelected, List<Category> subcategories) {
        showCustomFragment(SelectSubcategoryFragment.newInstanceForSelectedCategory(onSubcategorySelected, subcategories));
    }


    //show Fragment for Selecting Category
    public void showCustomFragment(SelectCategoryFragment fragment) {

        Log.d(TAG, "showCustomFragment(dialogFragment);");
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            transaction.remove(prev);
        }
        transaction.addToBackStack(null);

        fragment.show(transaction, "asd");

    }


    //show Fragment for Selecting Subcategory
    public void showCustomFragment(SelectSubcategoryFragment fragment) {


        Log.d(TAG, "showCustomFragment(dialogFragment);");
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            transaction.remove(prev);
        }
        transaction.addToBackStack(null);

        fragment.show(transaction, "dialog");

    }


}
