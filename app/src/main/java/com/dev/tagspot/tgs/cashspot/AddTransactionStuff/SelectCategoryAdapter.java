package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Utils.CategoriesAdapter;

import java.util.List;

/**
 * Created by Tgs on 6/30/2017.
 */

public class SelectCategoryAdapter extends BaseAdapter {

    private List<Category> categoryList;
    private Category category;
    private int selectedPosition;
    private OnItemClicked onItemClicked;

    public SelectCategoryAdapter(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public SelectCategoryAdapter() {
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_item, parent, false);
        }
        category = categoryList.get(position);

        final ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        final TextView textView = (TextView) convertView.findViewById(R.id.titleTv);
        textView.setText(category.getName());
        if(position == selectedPosition){
            convertView.setBackgroundColor(Color.GREEN);
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 selectedPosition = position;
                onItemClicked.onCategoryClicked(categoryList.get(position));
                notifyDataSetChanged();
            }
        });


        return convertView;
    }
}
