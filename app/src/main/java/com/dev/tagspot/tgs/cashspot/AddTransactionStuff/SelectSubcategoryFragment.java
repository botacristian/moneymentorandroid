package com.dev.tagspot.tgs.cashspot.AddTransactionStuff;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tgs on 7/3/2017.
 */

public class SelectSubcategoryFragment extends DialogFragment implements OnItemClicked {

    private static final String TAG = SelectSubcategoryFragment.class.getSimpleName();
    private GridView categoriesGridView;
    OnSubcategorySelected onSubcategorySelected;
    List<Category> subcategories = new ArrayList<>();

    public static SelectSubcategoryFragment newInstanceForSelectedCategory(OnSubcategorySelected onSubcategorySelected, List<Category> subcategories) {
        SelectSubcategoryFragment dialogFragment = new SelectSubcategoryFragment();
        dialogFragment.setOnSubcategorySelected(onSubcategorySelected);
        dialogFragment.subcategories = subcategories;
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    private void setOnSubcategorySelected(OnSubcategorySelected onSubcategorySelected) {
        this.onSubcategorySelected = onSubcategorySelected;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM;
            window.setAttributes(params);
        }
    }

    @Override
    public void onCategoryClicked(Category category) {

        Log.d(TAG, "category name= " + category.getName());
        onSubcategorySelected.onSubcategorySelected(category);
        this.dismiss();
    }


    interface OnSubcategorySelected {
        void onSubcategorySelected(Category c);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_category_dialog_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {

        categoriesGridView = (GridView) v.findViewById(R.id.gridView);
        SelectCategoryAdapter adapter = new SelectCategoryAdapter(subcategories);
        adapter.setOnItemClicked(SelectSubcategoryFragment.this);
        categoriesGridView.setAdapter(adapter);


//        Category.getCategoriesAsync(getActivity(), new Category.OnCategoriesRetrievedListener() {
//            @Override
//            public void onCategoriesRetrieved(List<Category> categories) {
//                if(categories != null){
//
//                    SelectCategoryAdapter adapter = new SelectCategoryAdapter(categories);
//                    adapter.setOnItemClicked(SelectSubcategoryFragment.this);
//                    categoriesGridView.setAdapter(adapter);
//                } else {
//
//                    //TODO show error message
//                }
//            }
//        });


    }
}
