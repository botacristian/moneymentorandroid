package com.dev.tagspot.tgs.cashspot.Categories;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.POJO.Subcategory;
import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 5/10/2017.
 */

public class AddCategoryDialogFragment extends DialogFragment implements View.OnClickListener, CategoriesIconFragment.OnIconPicked, NameFragment.OnNameSelected {

    private static final String TAG = AddCategoryDialogFragment.class.getSimpleName();
    View next, prev;
    ViewPager viewPager;
    Fragment firstPageFragment, secondPageFragment;
    int icon = 0;
    OnCategoryCreated onCategoryCreatedListener;
    OnSubcategoryCreated onSubcategoryCreatedListener;

    public static AddCategoryDialogFragment newInstanceForCategory(OnCategoryCreated onCategoryCreatedListener){
        AddCategoryDialogFragment dialogFragment = new AddCategoryDialogFragment();
        dialogFragment.setOnCategoryCreatedListener(onCategoryCreatedListener);

        return dialogFragment;
    }

    public static AddCategoryDialogFragment newInstanceForSubcategory(OnSubcategoryCreated onSubcategoryCreatedListener){
        AddCategoryDialogFragment dialogFragment = new AddCategoryDialogFragment();
        dialogFragment.setOnSubcategoryCreatedListener(onSubcategoryCreatedListener);

        return dialogFragment;
    }

    private void setOnSubcategoryCreatedListener(OnSubcategoryCreated onSubcategoryCreatedListener) {
        this.onSubcategoryCreatedListener = onSubcategoryCreatedListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog != null){
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM;
            window.setAttributes(params);

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.add_category_dialog_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        prev = v.findViewById(R.id.prev);
        next = v.findViewById(R.id.next);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);

        firstPageFragment = CategoriesIconFragment.newInstance(this);
        secondPageFragment = NameFragment.newInstance(this);

        viewPager = (ViewPager) v.findViewById(R.id.vp);
        viewPager.setAdapter(new MyPagerAdapter());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    prev.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                } else {
                    prev.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setOnCategoryCreatedListener(OnCategoryCreated onCategoryCreatedListener) {
        this.onCategoryCreatedListener = onCategoryCreatedListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:
                viewPager.arrowScroll(View.FOCUS_RIGHT);
                break;
            case R.id.prev:
                viewPager.arrowScroll(View.FOCUS_LEFT);

                break;
        }
    }

    @Override
    public void onIconPicked(int icon) {
        this.icon = icon;
        Log.d("onIconPicked", "icon = " + String.valueOf(icon));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        }, 250);
    }

    @Override
    public void onNameSelected(String name) {
        if(icon != 0){
            if(onCategoryCreatedListener != null){
                Log.d(TAG, "onCategoryCreatedListener != null");
                Category c = new Category(name, icon);
                c.setDeletable(true);
                onCategoryCreatedListener.onCategoryCreated(c);
            } else {
                Log.d(TAG, "onCategoryCreatedListener == null");
                Log.d(TAG, "name = " + name);
                Category c = new Category(name, icon);
                c.setDeletable(true);
                onSubcategoryCreatedListener.onSubCategoryCreated(c);
            }
            //TODO create category and return it to CategoriesFragment or create it there
        } else {
            icon = R.mipmap.ic_launcher;
            if(onCategoryCreatedListener != null) {
                Log.d(TAG, "2 :: onCategoryCreatedListener != null");
                Category c = new Category(name, icon);
                c.setDeletable(true);
                onCategoryCreatedListener.onCategoryCreated(c);
            } else {
                Log.d(TAG, "2 :: onCategoryCreatedListener == null");
                Category c = new Category(name, icon);
                c.setDeletable(true);
                onSubcategoryCreatedListener.onSubCategoryCreated(c);
            }
        }
        dismiss();
    }

    public interface OnCategoryCreated{
        void onCategoryCreated(Category c);
    }

    public interface OnSubcategoryCreated {
        void onSubCategoryCreated(Category c);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter{

        public MyPagerAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return firstPageFragment;
            }else{
                return secondPageFragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
