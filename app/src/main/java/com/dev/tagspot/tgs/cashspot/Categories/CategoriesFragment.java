package com.dev.tagspot.tgs.cashspot.Categories;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.OnItemLongTouch;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Utils.CategoriesAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tgs on 5/2/2017.
 */

public class CategoriesFragment extends Fragment implements OnItemClicked, View.OnClickListener, AddCategoryDialogFragment.OnCategoryCreated, AddCategoryDialogFragment.OnSubcategoryCreated, OnItemLongTouch {

    private static final String TAG = CategoriesFragment.class.getSimpleName();
    GridView categoryGridView, subcategoryGridView;
    Button addCategoryBtn, addSubcategoryBtn;
    List<Category> categories;

    CategoriesAdapter categoriesAdapter, subcategoriesAdapter;
    Category selectedCategory;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databaseReference;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.categories_fragment, container, false);
        init(v);

//        databaseReference = MainActivity.userIdReference.child("categories");

//        retrieveCategoriesAndSet();
        categories = MainActivity.categoriesList;
        setCategories(categories);

        return v;
    }

    private int getScreenHeight() {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }

    private void init(View v) {

        categoryGridView = (GridView) v.findViewById(R.id.categoryGridView);
        subcategoryGridView = (GridView) v.findViewById(R.id.subCategoryGridView);
        setViewHeight(categoryGridView);
        setViewHeight(subcategoryGridView);

        addCategoryBtn = (Button) v.findViewById(R.id.addCategoryBtn);
        addSubcategoryBtn = (Button) v.findViewById(R.id.addSubcategoryBtn);
        setButtonHeight(addCategoryBtn);
        setButtonHeight(addSubcategoryBtn);
        addCategoryBtn.setOnClickListener(this);
        addSubcategoryBtn.setOnClickListener(this);

//        checkIfCategoriesExist();


    }

    private void setViewHeight(View layout) {
        Log.e(TAG, "setViewHeight");
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        params.height = 26 * getScreenHeight() / 100;
    }

    private void setButtonHeight(Button button) {
        ViewGroup.LayoutParams params = button.getLayoutParams();
        params.height = 6 * getScreenHeight() / 100;
        Log.d(TAG, "button = " + params.height);
    }


    private void setCategories(List<Category> categories) {
        Log.d(TAG, "setCategories called");
        categoryGridView.setAdapter(null);
        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(getActivity());
        categoriesAdapter.setCategories(categories);
        categoriesAdapter.setOnItemClicked(this);
        categoriesAdapter.setOnItemLongTouch(this);

        categoryGridView.setAdapter(categoriesAdapter);
    }

    private void setSubcategories(ArrayList<Category> subcategories) {

        Log.d("CATEGORY", "setSubcategories  starts");

        if (subcategories != null) {
            Log.d("CATEGORY", "setSubcategories != null ");
            subcategoryGridView.setAdapter(null);
            subcategoriesAdapter = new CategoriesAdapter(getActivity());
            subcategoriesAdapter.setCategories(subcategories);
            subcategoriesAdapter.setOnItemLongTouch(this);
            subcategoryGridView.setAdapter(subcategoriesAdapter);
        } else {
            Log.d("CATEGORY", "setSubcategories == null ");
            subcategories = new ArrayList<>();
            subcategoryGridView.setAdapter(null);
            subcategoriesAdapter = new CategoriesAdapter(getActivity());
            subcategoriesAdapter.setCategories(subcategories);
            subcategoriesAdapter.setOnItemLongTouch(this);
            subcategoryGridView.setAdapter(subcategoriesAdapter);
        }

    }



    private void writeCategoriesToDb(Map<String, Category> categoryMap) {

        DatabaseReference reference = MainActivity.userIdReference.child("categories");
        reference.setValue(categoryMap);
        ((MainActivity) getActivity()).retrieveCategories();
    }


    private void getAndShowSubcategories(Category category) {
        Log.d("CATEGORIES", "getAndShowSubcategories");

        getAndSetCategorySubcategories(category);
    }

    private void getAndSetCategorySubcategories(Category category) {
        Log.e("CATEGORY", "getAndSetCategorySubcategories");
        final ArrayList<Category> categoryArrayList = new ArrayList<>();
        DatabaseReference reference = MainActivity.userIdReference;
        final Query subcategoriesQuery = reference.child("subcategories").orderByChild("parentId").equalTo(category.getId());
        subcategoriesQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.e(TAG, "dataSnapshot = " + dataSnapshot.toString());
                        Category c1 = dataSnapshot.getValue(Category.class);
                        categoryArrayList.add(c1);
//                    subcategoriesQuery.removeEventListener(this);
                }
                setSubcategories(categoryArrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO
            }
        });
//        subcategoriesQuery.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                if (dataSnapshot.exists()) {
//                    Log.e(TAG, "dataSnapshot = " + dataSnapshot.toString());
//                    Category c = dataSnapshot.getValue(Category.class);
//                    Log.d(TAG, "Category c = " + c.getName());
//                    categoryArrayList.add(c);
//                    setSubcategories(categoryArrayList);
//                    subcategoriesQuery.removeEventListener(this);
//                }
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });


//        setSubcategories(categoryArrayList);
//        Log.d(TAG, "getAndSetCategorySubcategories END");

//        setSubcategories(categoryArrayList);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addCategoryBtn:
                showAddCategoryDialogFragment();
                break;
            case R.id.addSubcategoryBtn:
                if (selectedCategory != null) {
                    AddCategoryDialogFragment dialogFragment = AddCategoryDialogFragment.newInstanceForSubcategory(CategoriesFragment.this);
                    showCustomFragment(dialogFragment);
                } else {
                    new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.please_select_category)).setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }

                break;

        }
    }

    private void showAddCategoryDialogFragment() {
        AddCategoryDialogFragment dialogFragment = AddCategoryDialogFragment.newInstanceForCategory(this);
        showCustomFragment(dialogFragment);
    }

    private void showCustomFragment(AddCategoryDialogFragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            transaction.remove(prev);
        }
        transaction.addToBackStack(null);

        fragment.show(transaction, "asd");


    }

    @Override
    public void onCategoryCreated(Category c) {
        setCategoryId(c);
        addCategoryToList(c);
        addCategoryToCategoriesMap();
    }

    private void setCategoryId(Category c) {
        int maxId = 0;
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getId() > maxId)
                maxId = categories.get(i).getId();
        }
        c.setId(maxId + 1);
        Log.d(TAG, "new category id = " + c.getId());
    }

    private void addCategoryToCategoriesMap() {

        Map<String, Category> categoryMap = new HashMap<String, Category>();
        for (int i = 0; i < categories.size(); i++) {
            categoryMap.put(categories.get(i).getName(), categories.get(i));
        }
//        categoryMap.put(c.getName(), c);
        Log.d(TAG, "addCategoryToCategoriesMap categories size = " + categories.size());
        writeCategoriesToDb(categoryMap);
    }

    private void addCategoryToList(Category c) {
        Log.d(TAG, "addCategoryToList() " + c.toString());
        Log.d(TAG, "addCategoryToList categories size = " + categories.size());
        categories.add(c);

        setCategories(categories);

    }

    @Override
    public void onSubCategoryCreated(Category c) {

        Log.d(TAG, "onSubCategoryCreated()" + c.getName());
        c.setParentId(selectedCategory.getId());
        selectedCategory.addSubcategory(c);
        getAndShowSubcategories(selectedCategory);
    }

    @Override
    public void onCategoryClicked(Category category) {
        if (selectedCategory != category) {
            selectedCategory = category;
            Log.e("CATEGORY", "onCategoryClicked");
            getAndShowSubcategories(category);
        }
    }

    @Override
    public void onCategoryLongTouch(Category category) {
        Log.d(TAG, "onCategoryLongTouch");
        if (category.isDeletable()) {
            Log.e(TAG, "isDeleteable");
            //TODO showDialog to delete
            deleteCategory(category);

        } else {

            Log.e(TAG, "is NOT Deleteable");
        }
    }

    private void removeChildren(Category category) {
        //TODO removeChildren not working as intended
        final ArrayList<Category> categoryArrayList = new ArrayList<>();
        Query query = MainActivity.userIdReference.child("subcategories").orderByChild("parentId").equalTo(category.getId());
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    Log.e(TAG, "dataSnps = " + dataSnapshot.toString());
                    Category c = dataSnapshot.getValue(Category.class);
                    Log.d(TAG, "Category to be removed = " + c.getName());
                    dataSnapshot.getRef().removeValue();
                    setSubcategories(null);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        final List<String> childIds = new ArrayList<>();
//        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("subcategories");
////        final Query subcategoriesQuery = reference.child("subcategories").orderByChild("parentId").equalTo(category.getId());
////        final Query subcategoriesQuery = reference.child("subcategories");
//
//        final ValueEventListener eventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                childIds.add(dataSnapshot.getKey());
//                setSubcategories(null);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };
//
//        reference.addValueEventListener(eventListener);
//        for (int i = 0; i < childIds.size(); i++) {
//            Log.e(TAG, "subcategories child = " + childIds.get(i));
//        }
//        reference.removeEventListener(eventListener);

//        ChildEventListener e = new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
////
//                Log.d(TAG, "onChildAdded dataSnapshot =" + dataSnapshot.toString());
//
//
//                childIds.add(dataSnapshot.getKey());
//                setSubcategories(null);
//                Log.d(TAG, "childId=s = " + childIds.size());
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };

//        subcategoriesQuery.addChildEventListener(e);
        Log.d(TAG, "onChildAdded done = TRUEEE");
//        subcategoriesQuery.removeEventListener(e);

    }

    private void deleteCategory(Category category) {

        if (category.getParentId() != -1) {
            //TODO remove only this category
            removeChildren(category);
        } else {
            //TODO remove all subcategories also  and then this category
//            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
//            final Query categoriesQuery = reference.child("categories").orderByChild("id").equalTo(category.getId()).limitToFirst(1);
            removeChildren(category);


//            categoriesQuery.addChildEventListener(new ChildEventListener() {
//                @Override
//                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                    if (dataSnapshot.exists()) {
//                        dataSnapshot.getRef().setValue(null);
//                        categoriesQuery.removeEventListener(this);
//                        retrieveCategoriesAndSet();
//                    }
//                }
//
//                @Override
//                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//                }
//
//                @Override
//                public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//                }
//
//                @Override
//                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//
        }

    }
}
