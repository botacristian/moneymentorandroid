package com.dev.tagspot.tgs.cashspot.Categories;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 5/11/2017.
 */

public class CategoriesIconFragment extends Fragment implements SimpleImageGridViewAdapter.OnItemClicked {

    OnIconPicked onIconPicked;
    GridView gridView;
    SimpleImageGridViewAdapter gridViewAdapter;
    int[] icons =
            {R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
                    R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round};


    public static CategoriesIconFragment newInstance(OnIconPicked onIconPicked) {
                CategoriesIconFragment iconFrag = new CategoriesIconFragment();
                iconFrag.setOnIconPickListener(onIconPicked);
        return iconFrag;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.categories_icon_fragment, container, false);

        init(v);
        return v;
    }

    private void setOnIconPickListener(OnIconPicked onIconPickListener) {
        this.onIconPicked = onIconPickListener;
    }

    private void init(View v) {
        gridView = (GridView) v.findViewById(R.id.gridView);
        gridViewAdapter = new SimpleImageGridViewAdapter(icons, getActivity(), this);
        gridView.setAdapter(gridViewAdapter);


    }

    @Override
    public void onIconClicked(int icon) {
        onIconPicked.onIconPicked(icon);
    }

    interface OnIconPicked {
        void onIconPicked(int icon);
    }
}
