package com.dev.tagspot.tgs.cashspot.Categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Cristi on 5/14/2017.
 */

public class NameFragment extends Fragment{

    OnNameSelected onNameSelected;

    public void setOnNameSelected(OnNameSelected onNameSelected) {
        this.onNameSelected = onNameSelected;
    }

    EditText name;
    Button done;
    String s;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.categories_iname_fragment, container, false);
        done = (Button) v.findViewById(R.id.done);
        name = (EditText) v.findViewById(R.id.category_name);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s = name.getText().toString();
                if(s.length() > 0){
                    onNameSelected.onNameSelected(s);

                } else {
                    name.setHint(getString(R.string.please_enter_name));
                }
            }
        });
        return v;
    }

    public static Fragment newInstance(OnNameSelected onNameSelected) {
        NameFragment n = new NameFragment();
        n.setOnNameSelected(onNameSelected);
        return n;
    }

    interface OnNameSelected{
        void onNameSelected(String name);
    }
}
