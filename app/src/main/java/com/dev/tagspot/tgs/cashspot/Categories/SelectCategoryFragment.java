package com.dev.tagspot.tgs.cashspot.Categories;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.SelectCategoryAdapter;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 6/30/2017.
 */

public class SelectCategoryFragment extends DialogFragment implements OnItemClicked {

    private static final String TAG = SelectCategoryFragment.class.getSimpleName();
    private OnCategorySelected onCategorySelectedListener;
    GridView categoriesGridView;

    public static SelectCategoryFragment newInstanceForSelectCategory(OnCategorySelected onCategorySelected) {
        SelectCategoryFragment dialogFragment = new SelectCategoryFragment();
        dialogFragment.setOnCategorySelectedListener(onCategorySelected);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void setOnCategorySelectedListener(OnCategorySelected onCategorySelected) {
        this.onCategorySelectedListener = onCategorySelected;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM;
            window.setAttributes(params);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_category_dialog_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {

        categoriesGridView = (GridView) v.findViewById(R.id.gridView);

        SelectCategoryAdapter adapter = new SelectCategoryAdapter(MainActivity.categoriesList);
        adapter.setOnItemClicked(SelectCategoryFragment.this);
        categoriesGridView.setAdapter(adapter);


//        Category.getCategoriesAsync(getActivity(), new Category.OnCategoriesRetrievedListener() {
//            @Override
//            public void onCategoriesRetrieved(List<Category> categories) {
//                if(categories != null){
//
//                    SelectCategoryAdapter adapter = new SelectCategoryAdapter(categories);
//                    adapter.setOnItemClicked(SelectCategoryFragment.this);
//                    categoriesGridView.setAdapter(adapter);
//                } else {
//
//                    //TODO show error message
//                }
//            }
//        });

    }

    @Override
    public void onCategoryClicked(Category category) {
        Log.d(TAG, "category name= " + category.getName());
        onCategorySelectedListener.onCategorySelected(category);
        dismiss();

    }


    public interface OnCategorySelected {
        void onCategorySelected(Category c);
    }


}
