package com.dev.tagspot.tgs.cashspot.Categories;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Tgs on 5/11/2017.
 */

public class SimpleImageGridViewAdapter extends BaseAdapter {
    private int[] icons;
    private Context context;
    private OnItemClicked onItemClicked;
    private int selectedPosition;

    public SimpleImageGridViewAdapter(int[] icons, Context context, OnItemClicked onItemClicked) {
        this.icons = icons;
        this.context = context;
        this.onItemClicked = onItemClicked;
    }

    @Override
    public int getCount() {
        return icons.length;
    }

    @Override
    public Object getItem(int position) {
        return icons[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if(convertView == null){
            imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setPadding(16, 16, 16, 16);
        } else {
            imageView = (ImageView) convertView;
        }
        if(selectedPosition == position){
            imageView.setBackgroundColor(Color.BLUE);
        } else {
            imageView.setBackgroundColor(Color.TRANSPARENT);
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                onItemClicked.onIconClicked(icons[position]);
                notifyDataSetChanged();
            }
        });
        imageView.setImageResource(icons[position]);
        return imageView;
    }

    public interface OnItemClicked {
        void onIconClicked(int icon);
    }

}
