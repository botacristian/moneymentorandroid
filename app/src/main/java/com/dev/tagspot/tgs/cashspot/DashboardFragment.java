package com.dev.tagspot.tgs.cashspot;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddExpenseManuallyFragment;
import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddIncomeManuallyFragment;
import com.dev.tagspot.tgs.cashspot.Transactions.IncompleteTransactionsHandler;
import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionAdd;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionSubtract;
import com.dev.tagspot.tgs.cashspot.Transactions.TransactionsAdapter;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Tgs on 3/31/2017.
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = DashboardFragment.class.getSimpleName();
    private Button transactionsRecyclerToggleBtn, cameraBtn, manuallyBtn, addIncomeBtn, addExpenseBtn;
    private PieChart pieChart;
    RecyclerView transactionsRecyclerView;
    Animation recyclerEnterAnimation, recyclerExitAnimation;
    Dialog cameraOrManualDialog;
    int h, w;
//    private static List<Transaction> transactionsList;

    static List<Transaction> transactionsList;
//    la intrarea in app si dupa fiecare tranzactie(inafara de transfer):
//
//
//    un query pe transactions/add si transactions/subtract ->
//    daca ii pe luna curenta si are categorie,
//    memorez numele categoriei,
//    suma,
//    si tipul tranzactiei

    static IncompleteTransactionsHandler incompleteTransactionsHandler;
    /*
    se ia data aleasa, si sa face un request pentru tranzactiile din ziua respectiva
     */

    private Button dateBtn;
    int mYear, mMonth, mDay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dashboard_fragment, container, false);

        pieChart = (PieChart) v.findViewById(R.id.pie_chart);
        transactionsRecyclerView = (RecyclerView) v.findViewById(R.id.transactionsRecyclerVie);
        transactionsRecyclerToggleBtn = (Button) v.findViewById(R.id.transactionsRecyclerToggleBtn);
        transactionsRecyclerToggleBtn.setOnClickListener(this);


        setupViews(v);
        setPieChartSize();
        setupTransactionRecycler();
        setUpAnimations();
        downloadAddTransactionList();


        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        tryToAddDataToChartOrWait();
    }

    private synchronized void downloadAddTransactionList() {
        showProgressDialog();
        final Query query = MainActivity.userIdReference.child("transactions").child("add").orderByChild("date").equalTo(getFormattedDateForBackend());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (transactionsList == null) {
                    transactionsList = new ArrayList<Transaction>();
                } else {
                    transactionsList.clear();
                }
                if (dataSnapshot.exists()) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Log.d(TAG, "dataSnapshot TransactionList = " + dataSnapshot1.toString());
                        Transaction t = dataSnapshot1.getValue(Transaction.class);
                        if (t.getType().equals(Transaction.ADD)) {
                            Log.d(TAG, "dataSnapshot1 ADD = " + dataSnapshot1.getKey());
                            TransactionAdd add = dataSnapshot1.getValue(TransactionAdd.class);
                            add.setId(dataSnapshot1.getKey());
                            transactionsList.add(add);
                        } else {
                            transactionsList.add(t);
                        }
                    }
                }
                downloadSubtractTransactionList();

                query.removeEventListener(this);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                dismissProgressDialog();
                query.removeEventListener(this);
            }
        });


    }

    private void downloadSubtractTransactionList() {
        showProgressDialog();
        final Query query = MainActivity.userIdReference.child("transactions").child("subtract").orderByChild("date").equalTo(getFormattedDateForBackend());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Log.d(TAG, "dataSnapshot TransactionList = " + dataSnapshot1.toString());
                        Transaction t = dataSnapshot1.getValue(Transaction.class);
                        if (t.getType().equals(Transaction.SUBTRACT)) {
                            Log.d(TAG, "dataSnapshot1 SUBTRACT = " + dataSnapshot1.getKey());
                            TransactionSubtract subtract = dataSnapshot1.getValue(TransactionSubtract.class);
                            subtract.setId(dataSnapshot1.getKey());
                            transactionsList.add(subtract);

                        } else {

                            transactionsList.add(t);
                        }
//                        t.setId(dataSnapshot1.getKey());
                        Log.d(TAG, "dataSnapshot1.getKey() = " + dataSnapshot1.getKey());
                    }
                }

                setTransactionsList(transactionsList);
                dismissProgressDialog();
                query.removeEventListener(this);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                dismissProgressDialog();
                query.removeEventListener(this);
            }
        });

    }

    private void showProgressDialog() {
        ((MainActivity) getActivity()).showProgressDialog();
    }

    private void setTransactionsList(final List<Transaction> transactionsList) {
        Log.d(TAG, "setTransactionsList" + " Size = " + transactionsList.toString());
        TransactionsAdapter adapter = new TransactionsAdapter((ArrayList<Transaction>) transactionsList);
        transactionsRecyclerView.setAdapter(adapter);

//
//        Handler h = new Handler();
//        h.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (incompleteTransactionsHandler == null) {
//                    //TODO this only with Incomplete Transactions
//                    incompleteTransactionsHandler = new IncompleteTransactionsHandler(transactionsList, getActivity());
//                    incompleteTransactionsHandler.getNextPost();
//                } else {
//                    incompleteTransactionsHandler.getNextPost();
//                }
//            }
//        }, 4000);

    }


    private void dismissProgressDialog() {
        ((MainActivity) getActivity()).dismissProgressDialog();
    }

    private void setupViews(View v) {
        addIncomeBtn = (Button) v.findViewById(R.id.addIncomeButton);
        addIncomeBtn.setOnClickListener(this);
        addExpenseBtn = (Button) v.findViewById(R.id.addExpenseButton);
        addExpenseBtn.setOnClickListener(this);
        mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        mMonth = Calendar.getInstance().get(Calendar.MONTH);
        mYear = Calendar.getInstance().get(Calendar.YEAR);
        dateBtn = (Button) v.findViewById(R.id.dateBtn);
        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mYear = year;
                        mMonth = month;
                        mDay = dayOfMonth;
                        updateDisplay();
                    }
                };

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), dateSetListener, mYear, mMonth, mDay);
                Calendar c = Calendar.getInstance();
                datePickerDialog.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        dateBtn.setText(getFormattedDateForUser());
    }

    private String getFormattedDateForUser() {

        GregorianCalendar g = new GregorianCalendar(mYear, mMonth, mDay);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

        return sdf.format(g.getTime());

    }

    private String getFormattedDateForBackend() {
        GregorianCalendar g = new GregorianCalendar(mYear, mMonth, mDay);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //Console Log purposes
        String v = sdf.format(g.getTime());

        Log.d(TAG, "getFormattedDateforBackend" + v);
        return v;
    }


    private void updateDisplay() {
        hideRecyclerView();
        dateBtn.setText(getFormattedDateForUser());
        downloadAddTransactionList();
        //set EditText correct date


    }

    private void setUpAnimations() {

        recyclerEnterAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.vertical_anim_in);
        recyclerExitAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.vertical_anim_out);
    }

    private void setupTransactionRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        transactionsRecyclerView.setLayoutManager(manager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        transactionsRecyclerView.addItemDecoration(dividerItemDecoration);
        transactionsRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        setTransactionsList(Tr);

    }

    private void setPieChartSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        h = metrics.heightPixels;
        w = metrics.widthPixels;
        Log.d(TAG, "height = " + h);
        Log.d(TAG, "width = " + w);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w / 2 + w / 4, h / 2 + h / 4);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        pieChart.setLayoutParams(params);

    }

    private void tryToAddDataToChartOrWait() {
        Log.d(TAG, "tryToAddDataToChartOrWait");
        Handler handler = new Handler();
        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (((MainActivity) getActivity()).isDownloadingCategories()) {
                        tryToAddDataToChartOrWait();
                    } else {
                        addDataToChart();
                    }

                }
            }, 1000);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("ERROR", "Dashboard Fragment try to Add Data to Chart");

        }

    }

    private void addDataToChart() {

        int[] colors = {R.color.color2, R.color.color3, R.color.color4, R.color.color5, R.color.color6, R.color.color7, R.color.color8, R.color.color9, R.color.color10, R.color.color11
                , R.color.color12, R.color.color13, R.color.color14, R.color.color15, R.color.color16};
        List<PieEntry> entries = new ArrayList<PieEntry>();
        for (int i = 0; i < MainActivity.categoriesList.size(); i++) {
            if (MainActivity.categoriesList.get(i).getAmountSpent() > 0) {
                entries.add(new PieEntry(MainActivity.categoriesList.get(i).getAmountSpent(), MainActivity.categoriesList.get(i).getName()));
                Log.d("CHART", "added entry = " + MainActivity.categoriesList.get(i).getName());
            }
        }

        if (entries.size() > 0) {
            PieDataSet set = new PieDataSet(entries, "stuff");
            set.setColors(colors, getActivity());
            PieData pieData = new PieData(set);

//        SpannableString spannableString = new SpannableString("Hello Man!");
//        BackgroundColorSpan greenBackground = new BackgroundColorSpan(Color.GREEN);
//        spannableString.setSpan(greenBackground, 0, spannableString.length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
//        //TODO set centerText from income and expenses
//        pieChart.setCenterText(spannableString);
//        pieChart.setCenterTextSize(20f);
            pieChart.setTransparentCircleRadius(55f);
//            pieChart.setUsePercentValues(true);
//        pieChart.setBackgroundColor(getResources().getColor(R.color.lt_gray));
            pieChart.setData(pieData);
            pieChart.invalidate();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.transactionsRecyclerToggleBtn:
                showOrHideRecyclerView();
                break;
            case R.id.addIncomeButton:
                MainActivity.typeOfTransactionIsIncome = true;
                showOrHidePickerDialog();
                break;
            case R.id.addExpenseButton:
                MainActivity.typeOfTransactionIsIncome = false;
                showOrHidePickerDialog();
                break;
            case R.id.cameraBtn:
                cameraOrManualDialog.dismiss();
//                ((MainActivity) getActivity()).changeFragment(new AddExpenseCameraFragment());
                ((MainActivity) getActivity()).getCameraPermission();
                Log.d(TAG, "cameraBtn");
                break;
            case R.id.manuallyBtn:
                cameraOrManualDialog.dismiss();
                if (MainActivity.typeOfTransactionIsIncome) {
                    ((MainActivity) getActivity()).changeFragment(new AddIncomeManuallyFragment());
                } else {
                    ((MainActivity) getActivity()).changeFragment(new AddExpenseManuallyFragment());
                }
                Log.d(TAG, "manuallyBtn");
                break;
        }
    }

    private void showOrHidePickerDialog() {
        if (cameraOrManualDialog == null) {
            cameraOrManualDialog = new Dialog(getActivity(), R.style.CustomDialogTheme);
            cameraOrManualDialog.setContentView(R.layout.expense_dialog_picker);

            cameraBtn = (Button) cameraOrManualDialog.findViewById(R.id.cameraBtn);
            manuallyBtn = (Button) cameraOrManualDialog.findViewById(R.id.manuallyBtn);
            cameraBtn.setOnClickListener(this);
            manuallyBtn.setOnClickListener(this);
            cameraOrManualDialog.show();
        } else {
            cameraOrManualDialog.show();
        }
    }

    private void hideRecyclerView() {
        if (transactionsRecyclerView.isShown()) {
            recyclerExitAnimation.reset();
            transactionsRecyclerView.startAnimation(recyclerExitAnimation);
            transactionsRecyclerView.setVisibility(View.GONE);
        }
    }

    private void showOrHideRecyclerView() {
        if (transactionsRecyclerView.isShown()) {
            recyclerExitAnimation.reset();
            transactionsRecyclerView.startAnimation(recyclerExitAnimation);
            transactionsRecyclerView.setVisibility(View.GONE);
        } else {
            recyclerEnterAnimation.reset();
            transactionsRecyclerView.startAnimation(recyclerEnterAnimation);
            transactionsRecyclerView.setVisibility(View.VISIBLE);
        }
    }
}
