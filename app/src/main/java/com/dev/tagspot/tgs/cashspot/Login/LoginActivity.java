package com.dev.tagspot.tgs.cashspot.Login;

//import io.fabric.sdk.android.Fabric;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Utils.CustomActivity;
import com.dev.tagspot.tgs.cashspot.Utils.KeyBoard;
import com.dev.tagspot.tgs.cashspot.Utils.UserSession;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Tgs on 4/11/2017.
 */

public class LoginActivity extends CustomActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private LoginButton loginButton;

    private UserSession session;
    private String email, pass;
    private FirebaseAuth auth;
    private CallbackManager callbackManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.login_activity);


        callbackManager = CallbackManager.Factory.create();
        auth = FirebaseAuth.getInstance();

        initViews();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();

        session = new UserSession(LoginActivity.this);
        if (session.isAuthenticated()) {
            launchMainActivity();
        }
    }

    private void launchMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    private void initViews() {

        (findViewById(R.id.submitBtn)).setOnClickListener(this);
        (findViewById(R.id.forgotPassTv)).setOnClickListener(this);
        (findViewById(R.id.signUpTv)).setOnClickListener(this);
        (findViewById(R.id.root)).setOnClickListener(this);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, loginResult.toString());
                Log.d(TAG, "onFacebook Success Login");

                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);


                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.i("LoginActivity", response.toString());
                            Log.i("LoginActivity", object.toString());
                            String email = object.getString("email");
                            Log.i("email", email);
                            createUserSession(email);
                            launchMainActivity();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                Log.d(TAG, error.toString());
            }
        });
    }

    private void createUserSession(String email) {
        if (session == null) {
            session = new UserSession(LoginActivity.this);
            session.createUserLoginSession(email);

        } else {
            session.createUserLoginSession(email);
        }

        //TODO Change user here

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.submitBtn) {
            if (isValidData()) {

                auth.signInWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d(TAG, "signInWithEmail:SUCCESS");
                                if (task.isSuccessful()) {
                                    session.createUserLoginSession(email);
                                    KeyBoard.hide(LoginActivity.this);
                                    launchMainActivity();

                                } else {

                                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                                    Toast.makeText(LoginActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
            } else {
                if (!isValidEmail(email)) {
                    findViewById(R.id.invalidEmailTv).setVisibility(View.VISIBLE);
                    findViewById(R.id.emailEt).requestFocus();

                } else if (!isValidPass(pass)) {
                    findViewById(R.id.invalidPassTv).setVisibility(View.VISIBLE);
                    findViewById(R.id.passwordEt).requestFocus();
                }
            }
        } else if (i == R.id.forgotPassTv) {
            forgotPassword();
        } else if (i == R.id.signUpTv) {
            signUp();
        } else if (i == R.id.root) {
            KeyBoard.hide(LoginActivity.this);
        }
    }

    private void signUp() {
        Log.d(TAG, "signUp");
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    private void forgotPassword() {
        Log.d(TAG, "forgotPassword()");
        //TODO

    }

    private boolean isValidData() {
        findViewById(R.id.invalidEmailTv).setVisibility(View.GONE);
        findViewById(R.id.invalidPassTv).setVisibility(View.GONE);
        email = ((EditText) findViewById(R.id.emailEt)).getText().toString().trim();
        pass = ((EditText) findViewById(R.id.passwordEt)).getText().toString().trim();
        return isValidEmail(email) && isValidPass(pass);
    }

    private boolean isValidPass(String pass) {
        return pass.length() >= 5;
    }

}
