package com.dev.tagspot.tgs.cashspot.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Utils.CustomActivity;
import com.dev.tagspot.tgs.cashspot.Utils.KeyBoard;
import com.dev.tagspot.tgs.cashspot.Utils.UserSession;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Tgs on 4/10/2017.
 */

public class RegisterActivity extends CustomActivity implements View.OnClickListener {

    //TODO: onClick pe rootLayout hide keyboard
    private FirebaseAuth mAuth;


    //LOGIN, Dialogs, lists/
    private static final String TAG = RegisterActivity.class.getSimpleName();

    UserSession session;

    String name, password, email;
    TextInputLayout tilName, tilPassword, tilPassword2, tilEmail;
    EditText etPassword, etName, etEmail, etPassword2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity_layout);
        init();
        mAuth = FirebaseAuth.getInstance();

    }


    private void init() {
        etName = (EditText) findViewById(R.id.nameEt);
        etPassword = (EditText) findViewById(R.id.passwordEt);
        etPassword2 = (EditText) findViewById(R.id.password2Et);
        etEmail = (EditText) findViewById(R.id.emailEt);
        tilName = (TextInputLayout) findViewById(R.id.nameLayout);
        tilPassword = (TextInputLayout) findViewById(R.id.passwordLayout);
        tilPassword2 = (TextInputLayout) findViewById(R.id.password2Layout);
        tilEmail = (TextInputLayout) findViewById(R.id.emailLayout);

        (findViewById(R.id.submitBtn)).setOnClickListener(this);
        (findViewById(R.id.root)).setOnClickListener(this);
        (findViewById(R.id.alreadyHaveAccountTv)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.submitBtn) {
            checkData();
//            String s = ((EditText)findViewById(R.id.nameEt)).getText().toString();
//            Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_LONG).show();
//            Log.d(TAG, "s = " + s);
        } else if (i == R.id.alreadyHaveAccountTv) {
            launchMainActivity();
        } else if (i == R.id.root) {
            KeyBoard.hide(RegisterActivity.this);
        }

    }


    private void launchMainActivity() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void checkData() {
        boolean allOk = true;
        tilName.setError(null);
        tilEmail.setError(null);
        tilPassword.setError(null);
        tilPassword2.setError(null);

        name = etName.getText().toString();
        password = etPassword.getText().toString();
        String password2 = etPassword2.getText().toString();
        email = etEmail.getText().toString();

        if (!isValidName()) {
            tilName.setError("Invalid name");
            allOk = false;
            etName.requestFocus();
        } else if (!isValidEmail(email)) {
            tilEmail.setError("Invalid email");
            allOk = false;
            etEmail.requestFocus();
        } else if (!isValidPassword(password, password2)) {
            tilPassword.setError("Invalid password(atleast 6 chars)");
            tilPassword2.setError("Invalid password(atleast 6 chars)");
            etPassword.requestFocus();
            allOk = false;
        }

        if (allOk) {
            Log.d(TAG, "allOk");

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, "Account succesfully created!", Toast.LENGTH_LONG).show();
                                Log.d(TAG, "signInWithEmail:success");
                                if (session == null) {
                                    session = new UserSession(RegisterActivity.this);
                                    session.createNewUser(email, name, password);
                                } else {
                                    session.createNewUser(email, name, password);
                                }
                                launchMainActivity();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Account creation error!", Toast.LENGTH_LONG).show();
                                Log.d(TAG, "signInWithEmail:error");
                            }
                        }
                    });


        } else {

            Log.d(TAG, "allOk NOT OK");
        }


    }

    private boolean isValidName() {
        return (name.length() > 0);
    }

    private boolean isValidPassword(String p1, String p2) {
        return p1.length() >= 5 && p1.equals(p2);
    }

}
