package com.dev.tagspot.tgs.cashspot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddExpenseCameraFragment;
import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddIncomeCameraFragment;
import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddIncomeManuallyFragment;
import com.dev.tagspot.tgs.cashspot.Categories.CategoriesFragment;
import com.dev.tagspot.tgs.cashspot.Notifications.MyFirebaseInstanceIDService;
import com.dev.tagspot.tgs.cashspot.Notifications.NotificationsFragment;
import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.Reports.ReportsFragment;
import com.dev.tagspot.tgs.cashspot.Settings.SettingsFragment;
import com.dev.tagspot.tgs.cashspot.Transfers.TransfersFragment;
import com.dev.tagspot.tgs.cashspot.Utils.KeyBoard;
import com.dev.tagspot.tgs.cashspot.Utils.UserSession;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class MainActivity extends AppCompatActivity {

    //TODO la On click pe transactions sa-ti deschide automat camera, si pe partea de sus a paginii sa poti sa introduci rapid valoarea
    //daca nu vrea sa se deschida automat sa seteze din setari
    //todo inverseaza butonul de + cu cel de -
    // flow: faci o poza, la sfarsitul zilei otificare care sa-ti zica sa-ti adaugi cheltuielile de pe ziua respectiva, si sa te duca intr-un fel de galerie unde fiecare poza o ai pe rand si
    // ti se deschide fragmentu de detaliu, unde-i actualizezi datele, te trece la urmatoarea, pana ai trecut pe toate din ziua de azi
    // apoi, sa te intrebe daca ai mai facut ceva cheltuieli, si la sfarsit sa te felicite si poate sa-ti dea un badge/ceva


    public static List<Account> accountList;
    public static List<Category> categoriesList;
    private static boolean hasAccounts;
    AccountsFragment accountsFragment;
    //    OnAccountsRetrieved onAccountsRetrieved;
    public static String userID;
    public static DatabaseReference userIdReference;
    private boolean isDownloadingAccounts = false;
    private boolean isDownloadingCategories = false;


    private static final String TAG = MainActivity.class.getSimpleName();
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    NavigationView navigationDrawer;
    private Handler handler;
    private final int DELAY = 250;

    private ProgressDialog d;

    private Uri fileUri;
    public static boolean typeOfTransactionIsIncome;
    //if false = spene
    //if true = income

    private ProgressDialog progressDialog;


    final PermissionCallback permissionCameraCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {

            Toast.makeText(MainActivity.this, "Access granted = true" , Toast.LENGTH_SHORT).show();
            getCameraPermission();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(MainActivity.this, "Access granted = false" , Toast.LENGTH_SHORT).show();
        }
    };


    final PermissionCallback permissionReadStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {

            Toast.makeText(MainActivity.this, "Access granted = true" , Toast.LENGTH_SHORT).show();
            getReadStoragePermission();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(MainActivity.this, "Access granted = false" , Toast.LENGTH_SHORT).show();
        }
    };

    final PermissionCallback permissionWriteStorageCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            getWriteStoragePermission();
            Toast.makeText(MainActivity.this, "permissionWriteStorageCallback Access granted = true" , Toast.LENGTH_SHORT).show();
        }

        @Override
        public void permissionRefused() {

            Toast.makeText(MainActivity.this, "permissionWriteStorageCallback Access granted = false" , Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(MainActivity.this);
        setUIdAndCreateUser();

        //Permissions lib
        Nammu.init(getApplicationContext());

        initViews();
        fragmentManager = getSupportFragmentManager();

        setupNavDrawer();


        retrieveAccounts();
        retrieveCategories();
        changeFragment(new DashboardFragment());

    }

    @Override
    protected void onResume() {
        super.onResume();
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(MainActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, MainActivity.this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Log.d(TAG, "picked image");
                fileUri = Uri.fromFile(new File(String.valueOf(imageFile)));
                Log.d(TAG, "file path = " + imageFile.toString());
                if(typeOfTransactionIsIncome){
                    Log.d(TAG, "typeOfTransactionIsIncome = false");
                    changeFragmentNoBackstack(getNewCameraAddIncomeFragment(fileUri));
                } else {
                    Log.d(TAG, "typeOfTransactionIsIncome = false");
                    changeFragmentNoBackstack(getNewCameraAddExpenseFragment(fileUri));
                }
            }
        });
    }


//    public Uri uploadPhotoToCloud(Uri filePath) {
    //TODO after user takes an image, the download link should be returned to the current fragment and attached to the post.
    //TODO case when he takes a photo from mainActivity with a UUID and in the next screen he changes the photo//
//        Log.e(TAG, "MAIN ACTIVITY uploadPhotoToCloud started");
//        FirebaseStorage storage = FirebaseStorage.getInstance();
//
//        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
//        StorageReference reference = storage.getReference().child(userId).child(UUID.randomUUID().toString());
//        StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpg").build();
//        UploadTask task = reference.putFile(filePath, metadata);
//        task.addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                e.printStackTrace();
//                Log.e("ERROR", "ERROR!!");
//                d.dismiss();
//            }
//        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//            @SuppressWarnings("VisibleForTests")
//            @Override
//            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//                downloadUrl = task.getResult().getDownloadUrl();
//                Log.e(TAG, "downloadUrl = " + downloadUrl.toString());
//            }
//        });
//
//        Log.e(TAG, "uploadPhotoToCloud ended");
//        return downloadUrl;
//
//    }

    private AddExpenseCameraFragment getNewCameraAddExpenseFragment(Uri fileUri){
            AddExpenseCameraFragment fragment = new AddExpenseCameraFragment();
        Bundle args = new Bundle();
        args.putString("uri", fileUri.toString());
        fragment.setArguments(args);

        return fragment;
    }

    private AddIncomeCameraFragment getNewCameraAddIncomeFragment(Uri fileUri){
        AddIncomeCameraFragment fragment = new AddIncomeCameraFragment();
        Bundle args = new Bundle();
        args.putString("uri", fileUri.toString());
        fragment.setArguments(args);

        return fragment;
    }


    void customDismissDialog() {
        if (!isDownloadingAccounts && !isDownloadingCategories) {
            try {
                dismissProgressDialog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isDownloadingCategories(){
        return isDownloadingCategories;
    }

    public void retrieveCategories() {
        if (categoriesList == null) {
            categoriesList = new ArrayList<>();
        } else {
            categoriesList.clear();
        }
        isDownloadingCategories = true;
        final DatabaseReference reference = userIdReference.child("categories");
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressWarnings("Since15")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(TAG, "dataSnapshot  = " + dataSnapshot.toString());
                categoriesList.clear();
                GenericTypeIndicator<HashMap<String, Category>> retrievedData = new GenericTypeIndicator<HashMap<String, Category>>() {
                };

                HashMap<String, Category> hashMap = (HashMap) dataSnapshot.getValue(retrievedData);
                if (hashMap != null) {
                    for (Category c : hashMap.values()) {
                        categoriesList.add(c);
                        Log.d(TAG, c.toString());
                    }

                    if (Build.VERSION.SDK_INT > 24) {
                        categoriesList.sort(new Comparator<Category>() {
                            @Override
                            public int compare(Category o1, Category o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    } else {
                        Collections.sort(categoriesList, new Comparator<Category>() {
                            @Override
                            public int compare(Category o1, Category o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    }

                    Log.e(TAG, "categories size = " + categoriesList.size());
                    isDownloadingCategories = false;
                    customDismissDialog();
//                    if (categories.size() > 0) {

//                    } else {

//                    }
                } else {
                    createCategories();
                }

                Log.d(TAG, "retrieveCategoriesAndSet removedEventListener");
                reference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO
                isDownloadingCategories = false;
                customDismissDialog();

            }
        });

    }

    private void createCategories() {

        Log.e(TAG, "createCategories");
        Category c1 = new Category("Category1", R.mipmap.ic_launcher);
        c1.setId(1);
        Category c2 = new Category("Category2", R.mipmap.ic_launcher);
        c2.setId(2);
        Category c3 = new Category("Category3", R.mipmap.ic_launcher);
        c3.setId(3);
        Category c4 = new Category("Category4", R.mipmap.ic_launcher);
        c4.setId(4);
        Category c5 = new Category("Category5", R.mipmap.ic_launcher);
        c5.setId(5);
        Category c6 = new Category("Category6", R.mipmap.ic_launcher);
        c6.setId(6);
        Category c7 = new Category("Category7", R.mipmap.ic_launcher);
        c7.setId(7);
        Category c8 = new Category("Category8", R.mipmap.ic_launcher);
        c8.setId(8);
        Category c9 = new Category("Category9", R.mipmap.ic_launcher);
        c9.setId(9);
        Category c10 = new Category("Category10", R.mipmap.ic_launcher);
        c10.setId(10);
        Category c11 = new Category("Category11", R.mipmap.ic_launcher);
        c11.setId(11);
        Category c12 = new Category("Category12", R.mipmap.ic_launcher);
        c12.setId(12);
        Category c13 = new Category("Category13", R.mipmap.ic_launcher);
        c13.setId(13);
        Category c14 = new Category("Category14", R.mipmap.ic_launcher);
        c14.setId(14);


        Map<String, Category> categoryMap = new HashMap<String, Category>();
        categoryMap.put(c1.getName(), c1);
        categoryMap.put(c2.getName(), c2);
        categoryMap.put(c3.getName(), c3);
        categoryMap.put(c4.getName(), c4);
        categoryMap.put(c5.getName(), c5);
        categoryMap.put(c6.getName(), c6);
        categoryMap.put(c7.getName(), c7);
        categoryMap.put(c8.getName(), c8);
        categoryMap.put(c9.getName(), c9);
        categoryMap.put(c10.getName(), c10);
        categoryMap.put(c11.getName(), c11);
        categoryMap.put(c12.getName(), c12);
        categoryMap.put(c13.getName(), c13);
        categoryMap.put(c14.getName(), c14);

        Log.d(TAG, "categoryHashMap size = " + categoryMap.size());


        DatabaseReference reference = userIdReference.child("categories");
        reference.setValue(categoryMap);

        retrieveCategories();
        Log.e(TAG, "END OF createCategories");
    }

    private void setUIdAndCreateUser() {
        userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.e(TAG, "userId= " + userID);
        userIdReference = FirebaseDatabase.getInstance().getReference("userdata").child(userID);

        Log.e(TAG,
                userIdReference.toString());
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);

      showProgressDialog();

    }

    public synchronized void showProgressDialog(){
        if(progressDialog != null){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        } else {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
        }

    }

    public synchronized void dismissProgressDialog(){
        Log.d(TAG, "dissmissProgressDialog called");
        if(progressDialog != null && progressDialog.isShowing()){
            Log.d(TAG, "dissmissProgressDialog != null && isShowing()");
            progressDialog.dismiss();
        }
    }


    private AccountsFragment createNewAccountsFragment() {
        accountsFragment = new AccountsFragment();
        return accountsFragment;
    }

    private AccountsFragment getAccountsFragment() {
        if (accountsFragment != null) {
            return accountsFragment;
        } else {
            return createNewAccountsFragment();
        }
    }

    private void setupNavDrawer() {

        navigationDrawer = (NavigationView) findViewById(R.id.left_drawer);
        navigationDrawer.setItemIconTintList(null);
        navigationDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:

                        drawerLayout.closeDrawers();
                        changeFragment(new DashboardFragment());
                        break;
                    case R.id.nav_accounts:

                        drawerLayout.closeDrawers();
                        changeFragment(getAccountsFragment());
                        break;
                    case R.id.nav_categories:

                        drawerLayout.closeDrawers();
                        changeFragment(new CategoriesFragment());
                        break;
                    case R.id.nav_reports:

                        drawerLayout.closeDrawers();
                        changeFragment(new ReportsFragment());
                        break;
                    case R.id.nav_transfers:

                        drawerLayout.closeDrawers();
                        changeFragment(new TransfersFragment());
                        break;
                    case R.id.nav_notifications:
                        drawerLayout.closeDrawers();
                        changeFragment(new NotificationsFragment());
                        break;
                    case R.id.nav_settings:

                        drawerLayout.closeDrawers();
                        changeFragment(new SettingsFragment());
                        break;
                    case R.id.nav_log_out:

                        drawerLayout.closeDrawers();
                        logOut();
                        break;

                }
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void changeFragment(final Fragment fragment) {
        Log.d(TAG, "changeFragment called ");
        try {
            KeyBoard.hideKeyboard(MainActivity.this);
            //TODO Change fragment only if other clicked
            if (handler == null) {
                handler = new Handler();
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    fragmentTransaction.replace(R.id.fragmentContainer, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }, DELAY);


        } catch (Exception e) {
            e.printStackTrace();
            e.getLocalizedMessage();
        }
    }

    public void changeFragmentNoBackstack(final Fragment fragment) {
        Log.d(TAG, "changeFragment called ");
        try {
            KeyBoard.hideKeyboard(MainActivity.this);

            if (handler == null) {
                handler = new Handler();
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    fragmentTransaction.replace(R.id.fragmentContainer, fragment);
                    fragmentTransaction.commit();

//                    java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
//                    at android.support.v4.app.FragmentManagerImpl.checkStateLoss(FragmentManager.java:1842)
//                    at android.support.v4.app.FragmentManagerImpl.enqueueAction(FragmentManager.java:1860)
//                    at android.support.v4.app.BackStackRecord.commitInternal(BackStackRecord.java:650)
//                    at android.support.v4.app.BackStackRecord.commit(BackStackRecord.java:609)
//                    at com.dev.tagspot.tgs.cashspot.MainActivity$8.run(MainActivity.java:557)
//                    at android.os.Handler.handleCallback(Handler.java:733)
//                    at android.os.Handler.dispatchMessage(Handler.java:95)
//                    at android.os.Looper.loop(Looper.java:136)
//                    at android.app.ActivityThread.main(ActivityThread.java:5590)
//                    at java.lang.reflect.Method.invokeNative(Native Method)
//                    at java.lang.reflect.Method.invoke(Method.java:515)
//                    at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1268)
//                    at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:1084)
//                    at dalvik.system.NativeStart.main(Native Method)
                }
            }, DELAY);


        } catch (Exception e) {
            e.printStackTrace();
            e.getLocalizedMessage();
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "backStackEntryCount = " + fragmentManager.getBackStackEntryCount());
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
            return;
        }
            finish();
            super.onBackPressed();
        }

    protected List<Account> getAndSetAccountList() {
        if (accountList == null) {
            accountList = new ArrayList<>();
        } else {
            accountList.clear();
        }
        return accountList;
    }

    public List<Account> getAccountList() {
        final List<Account> accountList1 = new ArrayList<>();
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("accounts");
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressWarnings("Since15")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                accountList.clear();
                GenericTypeIndicator<HashMap<String, Account>> retrievedData = new GenericTypeIndicator<HashMap<String, Account>>() {
                };

                HashMap<String, Account> hashMap = (HashMap) dataSnapshot.getValue(retrievedData);
                if (hashMap != null) {
                    for (Account c : hashMap.values()) {
                        accountList.add(c);
                        Log.d(TAG, c.toString());
                    }

                    if (Build.VERSION.SDK_INT > 24) {
                        accountList.sort(new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    } else {
                        Collections.sort(accountList, new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    }

                    Log.e(TAG, "accountList size = " + accountList.size());
                    if (accountList.size() > 0) {
//                        setCategories(categories);
                        Log.e(TAG, "accountList size = " + accountList1.size());
//                        accountsFragment.setAccounts(accountList1);
//                    if (accountList1.size() > 0) {
////                        setCategories(categories);
//                        hasAccounts = true;
//                        setAccounts(accountList);

                        if (accountsFragment != null) {
//                            accountsFragment.setAccounts(accountList);
                        }
                    }
                } else {
//                    createCategories();
//                    createAnAccount();
                }
                Log.d(TAG, "retrieveAccounts removedEventListener");
                reference.removeEventListener(this);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO

            }
        });

        return accountList;

    }

    public List<String> getAccountsNames() {
        List<String> strings = new ArrayList<>();
        if (MainActivity.accountList.size() > 0) {
            for (int i = 0; i < MainActivity.accountList.size(); i++) {
                strings.add(MainActivity.accountList.get(i).getName());
            }
        }
        Log.d(TAG, "getAccountsNames size = " + strings.size());
        return strings;
    }

    public void retrieveAccounts() {
        isDownloadingAccounts = true;
        if (accountList == null) {
            accountList = new ArrayList<>();
        } else {
            accountList.clear();
        }

        final DatabaseReference reference = userIdReference.child("accounts");
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressWarnings("Since15")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                accountList.clear();
                GenericTypeIndicator<HashMap<String, Account>> retrievedData = new GenericTypeIndicator<HashMap<String, Account>>() {
                };

                HashMap<String, Account> hashMap = (HashMap) dataSnapshot.getValue(retrievedData);
                if (hashMap != null) {
                    for (Account c : hashMap.values()) {
                        accountList.add(c);
                        Log.d(TAG, c.toString());
                    }

                    if (Build.VERSION.SDK_INT > 24) {
                        accountList.sort(new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    } else {
                        Collections.sort(accountList, new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    }

                    Log.e(TAG, "accountList size = " + accountList.size());
                    if (accountList.size() > 0) {
                        hasAccounts = true;

                        isDownloadingAccounts = false;
                        customDismissDialog();
//                        try{
//                            onAccountsRetrieved.onRetrieval(accountList);
//                        } catch (Exception e){
//                            e.printStackTrace();
//                        }
//                        setAccounts(accountList);

                    }
                } else {
                    isDownloadingAccounts = false;
                    customDismissDialog();
                    changeFragmentNoBackstack(getAccountsFragment());
//                    createCategories();
//                    createAnAccount();
                }
                Log.d(TAG, "retrieveAccounts removedEventListener");
                reference.removeEventListener(this);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO

                isDownloadingAccounts = false;
                customDismissDialog();

            }
        });
    }

    public synchronized void retrieveAccountsAndSetAccountsFragment() {

        showProgressDialog();
        if (accountList == null) {
            accountList = new ArrayList<>();
        } else {
            accountList.clear();
        }
        final DatabaseReference reference = userIdReference.child("accounts");
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressWarnings("Since15")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                accountList.clear();
                GenericTypeIndicator<HashMap<String, Account>> retrievedData = new GenericTypeIndicator<HashMap<String, Account>>() {
                };

                HashMap<String, Account> hashMap = (HashMap) dataSnapshot.getValue(retrievedData);
                if (hashMap != null) {
                    for (Account c : hashMap.values()) {
                        accountList.add(c);
                        Log.d(TAG, c.toString());
                    }

                    if (Build.VERSION.SDK_INT > 24) {
                        accountList.sort(new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    } else {
                        Collections.sort(accountList, new Comparator<Account>() {
                            @Override
                            public int compare(Account o1, Account o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                    }

                    Log.e(TAG, "accountList size = " + accountList.size());
                    if (accountList.size() > 0) {
                        hasAccounts = true;

                        changeFragmentNoBackstack(new AccountsFragment());

                    }
                } else {
                    changeFragmentNoBackstack(getAccountsFragment());
                }

                Log.d(TAG, "retrieveAccounts removedEventListener");
                reference.removeEventListener(this);
//                if (d.isShowing()) {
//                    d.dismiss();
//                }
                dismissProgressDialog();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO
                if (d.isShowing()) {
                    d.dismiss();
                }

            }
        });
    }

    private void logOut() {

        FirebaseAuth.getInstance().signOut();
        UserSession s = new UserSession(MainActivity.this);
        s.logoutUser();
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();
    }

    public static Account getAccountByName(String name) {
        //TODO what happens when two accounts have the same name?
        try {
            for (Account a : accountList) {
                if (a.getName().equals(name))
                    return a;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();

        }
        return null;
    }


    private void customTakePic() {
        EasyImage.openCamera(MainActivity.this, 0);
    }

    private void getWriteStoragePermission() {
        if(Nammu.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
//            viewSnackBar("Permission allowed", spinner);
            customTakePic();
        } else {
            if(Nammu.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)){
                Snackbar.make(drawerLayout, "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionWriteStorageCallback);
            }
        }
    }


    private void getReadStoragePermission() {
        if(Nammu.checkPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)){
            getWriteStoragePermission();
        } else {
            if(Nammu.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)){
                Snackbar.make(drawerLayout, "Grant permission to access the READ_EXTERNAL_STORAGE",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE, permissionReadStorageCallback);
            }
        }
    }


    public void  getCameraPermission() {
        if(Nammu.checkPermission(android.Manifest.permission.CAMERA)){
            getReadStoragePermission();
        } else {
            if(Nammu.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.CAMERA)){
                Snackbar.make(drawerLayout, "Grant permission to access the camera",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.CAMERA, permissionCameraCallback);
                            }
                        }).show();

            } else {
                Nammu.askForPermission(MainActivity.this, android.Manifest.permission.CAMERA, permissionCameraCallback);
            }
        }
    }

}
