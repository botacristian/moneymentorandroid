package com.dev.tagspot.tgs.cashspot.Notifications;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tgs on 9/20/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.CustomViewHolder> {

    List<Notification> notificationList;

    public NotificationsAdapter() {
    }

    public NotificationsAdapter(Notification d) {
        List<Notification> notifications = new ArrayList<>();
        notifications.add(d);
        this.notificationList = notifications;
    }

    public NotificationsAdapter(List<Notification> notificationList) {
        this.notificationList = notificationList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_li, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.title.setText("hasdahsas");
    }

    @Override
    public int getItemCount() {
        if(notificationList != null){
            return notificationList.size() - 1;
        }
        return 0;
    }

    static class CustomViewHolder extends RecyclerView.ViewHolder{
        TextView title;


        public CustomViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.titleTv);
        }
    }

}
