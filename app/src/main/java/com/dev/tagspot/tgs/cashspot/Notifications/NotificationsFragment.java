package com.dev.tagspot.tgs.cashspot.Notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 9/20/2017.
 */

public class NotificationsFragment extends Fragment {

    RecyclerView recyclerView;
    NotificationsAdapter notificationsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notifications_fragment, container, false);

        init(v);

        return v;
    }

    private void init(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.notificationsRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        notificationsAdapter = new NotificationsAdapter(new Notification());
        recyclerView.setAdapter(notificationsAdapter);

    }
}
