package com.dev.tagspot.tgs.cashspot;

import com.dev.tagspot.tgs.cashspot.POJO.Category;

/**
 * Created by Tgs on 6/30/2017.
 */

public interface OnItemClicked {
    void onCategoryClicked(Category category);
}
