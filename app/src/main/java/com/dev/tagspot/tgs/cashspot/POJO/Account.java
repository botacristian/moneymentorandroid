package com.dev.tagspot.tgs.cashspot.POJO;

import android.util.Log;

import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Tgs on 3/31/2017.
 */

public class Account {

    private static final String TAG = Account.class.getSimpleName();
    private String name;
    private float sum, budget;
    private boolean favourite;

    public Account() {

    }


    public void subtractFromAccount(float value) {
        Log.d(TAG, "sum before subtracting = " + String.valueOf(sum));
        sum -= value;
        Log.d(TAG, "sum after subtracting = " + String.valueOf(sum));
    }

    public Account subtractFromAccountWithReturn(float value) {
        Log.d(TAG, "sum before subtracting = " + String.valueOf(sum));
        sum -= value;
        Log.d(TAG, "sum after subtracting = " + String.valueOf(sum));
        return this;
    }

    public void addToAccount(float value) {
        Log.d(TAG, "sum before adding = " + String.valueOf(sum));
        sum += value;
        Log.d(TAG, "sum after adding = " + String.valueOf(sum));
    }

    public Account addToAccountWithReturn(float value) {
        Log.d(TAG, "sum before adding = " + String.valueOf(sum));
        sum += value;
        Log.d(TAG, "sum after adding = " + String.valueOf(sum));
        return this;
    }

    public Account(String name, float sum) {
        this.name = name;
        this.sum = sum;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public void updateAccountSumInDB() {
        Log.d(TAG, "started updateAccountSumInDB");
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        Query query = reference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    child.getRef().child("sum").setValue(sum);
                    Log.d(TAG, "onDataChange with new amount = " + sum);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateAccountSumInDB(final CustomSuccessCallback callback) {
        Log.d(TAG, "started updateAccountSumInDB");
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        Query query = reference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    child.getRef().child("sum").setValue(sum);
                    Log.d(TAG, "onDataChange with new amount = " + sum);
                    callback.onSuccess();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateAccountNameAndSum(final String newName, final float value, final CustomSuccessCallback callback) {
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        Query query = reference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    child.getRef().child("name").setValue(newName);
                    child.getRef().child("sum").setValue(value);
                    Log.d(TAG, "onDataChange with new amount = " + value);
                    callback.onSuccess();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError();
            }
        });
    }

    public void updateAccountSumInDB(final Float value, final CustomSuccessCallback callback) {
        Log.d(TAG, "started updateAccountSumInDB");
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        Query query = reference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    child.getRef().child("sum").setValue(value);
                    Log.d(TAG, "onDataChange with new amount = " + value);
                    callback.onSuccess();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError();
            }
        });
    }


    public void updateAccountName(final String accName, final CustomSuccessCallback callback) {
        //TODO after querying this, you must query every transaction in this account was made and edit the name there too
        Log.d(TAG, "started updateAccountSumInDB");
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        Query query = reference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    child.getRef().child("name").setValue(accName);
                    Log.d(TAG, "onDataChange with new name = " + accName);
                    callback.onSuccess();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError();

            }
        });
    }

    public void deleteAccount(Account account, final CustomSuccessCallback callback) {
        Log.d(TAG, "started deleteAccount");
        final DatabaseReference reference = MainActivity.userIdReference.child("accounts");
        reference.orderByChild("name").equalTo(account.getName()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap :
                        dataSnapshot.getChildren()) {

                            snap.getRef().removeValue();
                            callback.onSuccess();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError();
            }
        });

    }


}
