package com.dev.tagspot.tgs.cashspot.POJO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Transactions.Transaction;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Cristi on 5/7/2017.
 */

public class Category implements Serializable {

    private static final String TAG = Category.class.getSimpleName();
    private String name;
    private int id;
    private int icon = 0;
    private int parentId = -1;
    private boolean isDeletable = false;
    private float amountSpent = 0;

//    DatabaseReference childRef = ref.child("subcategories");

//    public Category(String name, int icon, ArrayList<Subcategory> subcategoryList, boolean hasSubcategories) {
//        this.name = name;
//        this.icon = icon;
//        this.subcategoryList = subcategoryList;
//        this.hasSubcategories = hasSubcategories;
//    }

//    //TODO to be deleted is if deletable onLongClick
//    public boolean isHasSubcategories() {
//        return hasSubcategories;
//    }
//
//    public void setHasSubcategories(boolean hasSubcategories) {
//        this.hasSubcategories = hasSubcategories;
//    }


    public float getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(float amountSpent) {
        this.amountSpent = amountSpent;
    }

    public void addToSpentAmount(float amountToAdd) {
        amountSpent = amountToAdd + amountSpent;
    }

    public void subtractFromSpentAmount(float amount) {
        amountSpent = amountSpent - amount;
    }


    public boolean isDeletable() {
        //TODO Check
        Log.d(TAG, "isDeleteable = " + isDeletable);
        return parentId != -1 || isDeletable;
//        if(parentId != -1){
//            Log.d(TAG, "parentId != -1 ");
//            return true;
//        } else if (isDeletable){
//            return true;
//        }
//        return false;
    }

    public void setDeletable(boolean deletable) {
        isDeletable = deletable;
    }

    public Category(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }


    public boolean hasIcon() {
        return icon != 0;
    }


    public Drawable getIconDrawable(Context c) {
        Drawable d = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            d = c.getResources().getDrawable(icon, null);
        } else {
            d = c.getResources().getDrawable(icon);
        }
        Log.d("icon = ", String.valueOf(icon));

        return d;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static Category fromJson(String json) {
        return new Gson().fromJson(json, Category.class);
    }


    public int getIcon() {
        return icon;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }


    public static ArrayList<Category> generateCategories(int n) {
        ArrayList<Category> categories = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            Category c;
            if (i % 2 == 0) {
                c = new Category(UUID.randomUUID() + "");
                c.setIcon(R.mipmap.ic_launcher);
            } else {
                c = new Category(UUID.randomUUID() + " i%2!=0");
                c.setIcon(R.mipmap.ic_launcher_round);
            }
            Log.e("Category", c.getName());
            categories.add(c);
        }

        return categories;
    }

    private void setIcon(int id) {
        icon = id;
    }


    public Category withName(String name) {
        this.name = name;
        return this;
    }

    public Category withIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public Category withParentId(int id) {
        this.parentId = id;
        return this;
    }


    public void addSubcategory(Category c) {
        DatabaseReference reference = MainActivity.userIdReference.child("subcategories");
        String id = reference.push().getKey();

        reference.child(id).setValue(c);


        Log.d(TAG, "sub category = " + c.toString());
        Log.d(TAG, "addSubcategory done");
    }

    public static void getCategoriesAsync(Activity activity, final OnCategoriesRetrievedListener listener) {
        final List<Category> categoryList = new ArrayList<>();
        final ProgressDialog d = new ProgressDialog(activity);
        d.setMessage("Loading");
//        d.getWindow().setGravity(Gravity.BOTTOM);
        d.show();
//        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
//        params.gravity = Gravity.BOTTOM;
//        activity.getWindow().setAttributes(params);
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {

                final DatabaseReference reference = MainActivity.userIdReference.child("categories");
                reference.addValueEventListener(new ValueEventListener() {
                    @SuppressWarnings("Since15")
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Log.d(TAG, "dataSnapshot  = " + dataSnapshot.toString());
                        categoryList.clear();
                        GenericTypeIndicator<HashMap<String, Category>> retrievedData = new GenericTypeIndicator<HashMap<String, Category>>() {
                        };

                        HashMap<String, Category> hashMap = (HashMap) dataSnapshot.getValue(retrievedData);
                        if (hashMap != null) {
                            for (Category c : hashMap.values()) {
                                categoryList.add(c);
                                Log.d(TAG, c.toString());
                            }

                            if (Build.VERSION.SDK_INT > 24) {
                                categoryList.sort(new Comparator<Category>() {
                                    @Override
                                    public int compare(Category o1, Category o2) {
                                        return o1.getName().compareTo(o2.getName());
                                    }
                                });
                            } else {
                                Collections.sort(categoryList, new Comparator<Category>() {
                                    @Override
                                    public int compare(Category o1, Category o2) {
                                        return o1.getName().compareTo(o2.getName());
                                    }
                                });
                            }

                            Log.e(TAG, "categories size = " + categoryList.size());
                            listener.onCategoriesRetrieved(categoryList.isEmpty() ? null : categoryList);
                        } else {
                            listener.onCategoriesRetrieved(null);
                        }
                        d.dismiss();
                        reference.removeEventListener(this);
                        Log.d(TAG, "getCategoriesAsync removedEventListener");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //TODO

                    }
                });
            }
        }, 1500);
    }

    public void getSubcategoriesAsync(Activity activity, final OnSubcategoriesRetrievedListener listener) {
        final List<Category> categoryList = new ArrayList<>();
        final ProgressDialog d = new ProgressDialog(activity);
        d.setMessage("Loading");
        d.show();
        final DatabaseReference reference = MainActivity.userIdReference;

        Query subcategoriesQuery = reference.child("subcategories").orderByChild("parentId").equalTo(getId());
        subcategoriesQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    Log.d(TAG, "onChildAdded");
                    Category c = dataSnapshot.getValue(Category.class);
                    categoryList.add(c);
                    Log.e(TAG, "subcategories size = " + categoryList.size());

                    listener.onSubcategoriesRetrieved(categoryList.isEmpty() ? null : categoryList);
                    d.dismiss();
                    reference.removeEventListener(this);

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        /*
        astea sunt aici pentru situatia cand nu are categoria subcategorii si nu  mai dispare loading dialogul
        TODO fix this shit
         */
        Log.e("onChildAdded", "onSubcategoriesRetrieved null");
        listener.onSubcategoriesRetrieved(null);
        d.dismiss();


    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getParentId() {
        return parentId;
    }

    public void addAndUpdateCategory(float value) {
        this.amountSpent = amountSpent + value;
        refreshCategory();
    }

    private void refreshCategory() {
        for (int i = 0; i < MainActivity.categoriesList.size(); i++) {
            if (MainActivity.categoriesList.get(i).getId() == this.id) {
                MainActivity.categoriesList.remove(i);
                Log.d(TAG, MainActivity.categoriesList.get(MainActivity.categoriesList.size() - 1).toString());
                MainActivity.categoriesList.add(Category.this);
                Log.d(TAG, MainActivity.categoriesList.get(MainActivity.categoriesList.size() - 1).toString());
                addCategoryToCategoriesMap();
                return;
            }
        }

    }

    private void addCategoryToCategoriesMap() {

        Map<String, Category> categoryMap = new HashMap<String, Category>();
        for (int i = 0; i < MainActivity.categoriesList.size(); i++) {
            categoryMap.put(MainActivity.categoriesList.get(i).getName(), MainActivity.categoriesList.get(i));
        }
//        categoryMap.put(c.getName(), c);
        Log.d(TAG, "addCategoryToCategoriesMap categories size = " + MainActivity.categoriesList.size());
        writeCategoriesToDb(categoryMap);
    }

    private void writeCategoriesToDb(Map<String, Category> categoryMap) {
        DatabaseReference reference = MainActivity.userIdReference.child("categories");
        reference.setValue(categoryMap);
//        ((MainActivity) getActivity()).retrieveCategories();
    }

    public void updateCategory(float value, String type) {
        if (type.equals(Transaction.ADD)) {
            addAndUpdateCategory(value);
        } else {
            subtractAndUpdateCategory(value);
        }
    }

    public void subtractAndUpdateCategory(float value) {
        this.amountSpent = amountSpent - value;
        refreshCategory();
    }


    public interface OnSubcategoriesRetrievedListener {
        void onSubcategoriesRetrieved(List<Category> subcategories);
    }

    public interface OnCategoriesRetrievedListener {
        void onCategoriesRetrieved(List<Category> categories);
    }

}
