package com.dev.tagspot.tgs.cashspot.POJO;

import android.util.Log;

/**
 * Created by Tgs on 5/9/2017.
 */

public class Subcategory {
    private String name;
    private int icon;

    public Subcategory(String s) {
        this.name = s;

    }
    public Subcategory() {
    }

    public Subcategory(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        if(name == null){
            Log.e("Subcategory", "name == null");
            return "";
        }
        Log.e("Subcategory", "name == " + name);
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
