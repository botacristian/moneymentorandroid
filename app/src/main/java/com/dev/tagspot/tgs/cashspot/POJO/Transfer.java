package com.dev.tagspot.tgs.cashspot.POJO;

/**
 * Created by Tgs on 6/29/2017.
 */

public class Transfer {
    public float amount;
    public String originAccountName, destinationAccountName;

    public Transfer(float amount, String originAccountName, String destinationAccountName) {
        this.amount = amount;
        this.originAccountName = originAccountName;
        this.destinationAccountName = destinationAccountName;

    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getOriginAccountName() {
        return originAccountName;
    }

    public void setOriginAccountName(String originAccountName) {
        this.originAccountName = originAccountName;
    }

    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }
}
