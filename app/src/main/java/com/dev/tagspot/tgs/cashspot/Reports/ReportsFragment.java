package com.dev.tagspot.tgs.cashspot.Reports;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 5/2/2017.
 */

public class ReportsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.reports_fragment, container, false);

        return v;
    }
}
