package com.dev.tagspot.tgs.cashspot.RewardsGamification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dev.tagspot.tgs.cashspot.DashboardFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 9/19/2017.
 */

public class IncompleteTransactionsDoneFragment extends Fragment {

    private Button finishButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.incomplete_transactions_done_fragment, container, false);

        init(v);

        return v;
    }

    private void init(View v) {
        finishButton = (Button) v.findViewById(R.id.finishButton);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(new DashboardFragment());
            }
        });

    }
}
