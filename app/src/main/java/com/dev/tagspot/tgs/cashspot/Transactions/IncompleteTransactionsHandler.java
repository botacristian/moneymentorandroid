package com.dev.tagspot.tgs.cashspot.Transactions;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddExpenseManuallyFragment;
import com.dev.tagspot.tgs.cashspot.AddTransactionStuff.AddIncomeManuallyFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.RewardsGamification.IncompleteTransactionsDoneFragment;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;

import java.util.List;

/**
 * Created by Tgs on 9/13/2017.
 */

public class IncompleteTransactionsHandler implements CustomSuccessCallback {

    private static final String TAG = IncompleteTransactionsHandler.class.getSimpleName();
    private List<Transaction> transactionList;
    private Context context;
    int i = 0;

    public IncompleteTransactionsHandler() {
    }

    public IncompleteTransactionsHandler(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public IncompleteTransactionsHandler(List<Transaction> transactionList, Context context) {
        this.transactionList = transactionList;
        this.context = context;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    private void startNewAddExpenseFragment() {

    }

    private void startNewAddIncomeFragment() {

    }

    private boolean hasUpdatedPost() {
        //todo
        return false;
    }


    private void showPost(Transaction transaction, CustomSuccessCallback callback) {
        Log.d(TAG, "showPost()");
        if (transaction != null) {

            if (transaction.getType().equals(Transaction.ADD)) {
                Fragment f = AddIncomeManuallyFragment.newInstance(transaction, callback);
                ((MainActivity) context).changeFragmentNoBackstack(f);
                //todo
                Log.d(TAG, "showPost() get type = ADD");
            } else if (transaction.getType().equals(Transaction.SUBTRACT)) {

                Log.d(TAG, "showPost() get type = SUBTRACT");
                Fragment f = AddExpenseManuallyFragment.newInstance(transaction, callback);
                ((MainActivity) context).changeFragmentNoBackstack(f);
            } else if (transaction.getType().equals(Transaction.TRANSFER)) {
                Log.d(TAG, transaction.getType());
                callback.onSuccess();
            }
        }
    }

    public void getNextPost() {
        Log.d(TAG, "getNextPost");
        if (transactionList != null) {
            showPost(getCurrentTransaction(), IncompleteTransactionsHandler.this);
        }
    }

    public Transaction getCurrentTransaction() {
        if (i < transactionList.size()) {
            return transactionList.get(i);
        }
        return null;
    }

    @Override
    public void onSuccess() {
        Log.d(TAG, "onSuccess()");
        if (i < transactionList.size() - 1) {
            Log.d(TAG, "i < transactionList.size() - 1");
            ++i;
            getNextPost();
        } else {
            Log.d(TAG, "i >= transactionList.size() - 1");
            ((MainActivity) context).changeFragmentNoBackstack(new IncompleteTransactionsDoneFragment());
        }
        //TODO
    }

    @Override
    public void onError() {
        Log.d(TAG, "onError()");
        //onError
    }
}
