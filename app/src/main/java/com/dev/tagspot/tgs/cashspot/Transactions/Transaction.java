package com.dev.tagspot.tgs.cashspot.Transactions;

import android.os.Parcel;
import android.os.Parcelable;

import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Tgs on 3/31/2017.
 */

public class Transaction implements Parcelable {


    public static final String SUBTRACT = "Subtract";
    public static final String TRANSFER = "Transfer";
    public static final String ADD = "Add";

    public static final String FLAG_INCOMPLETE = "Incomplete";
    public static final String FLAG_COMPLETE = "Complete";


    private float value;

    protected String name;
    private String id;
    protected String photoUID;
    protected String photoURL;

    public int hour, minute;

    private String destinationAccountName;
    private String type;
    protected String date;
    private Account destinationAccount;
    private int frequency;


    protected Transaction(Parcel in) {
        value = in.readFloat();
        hour = in.readInt();
        minute = in.readInt();
        name = in.readString();
        id = in.readString();
        photoUID = in.readString();
        photoURL = in.readString();
        destinationAccountName = in.readString();
        type = in.readString();
        date = in.readString();
        frequency = in.readInt();
        isRecursive = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(photoUID);
        dest.writeString(photoURL);
        dest.writeString(destinationAccountName);
        dest.writeString(type);
        dest.writeString(date);
        dest.writeInt(frequency);
        dest.writeInt(hour);
        dest.writeInt(minute);
        dest.writeFloat(value);
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public boolean isTransactionCompleted(String flag) {
        if (flag.equals(FLAG_COMPLETE)) {
            return true;
        } else {
            return false;
        }
    }

    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }


    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhotoUID() {
        return photoUID;
    }

    public void setPhotoUID(String photoUID) {
        this.photoUID = photoUID;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    private boolean isRecursive;
    //TODO save in sharedPrefs or Local Db recursive transactions and check if any date == todayDate && if transaction has been consumed today

    public Transaction() {
    }

    @Override
    public String toString() {
//        return name + "" + String.valueOf(value) + " " + date;

        return new Gson().toJson(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public static class TransactionGenerator {
        public static ArrayList<Transaction> generateTransactions(int n) {
            ArrayList<Transaction> transactions = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                Transaction t = new Transaction();
                t.setName("asd");
                t.setValue(123);
                transactions.add(t);
            }
            return transactions;
        }
    }


}
