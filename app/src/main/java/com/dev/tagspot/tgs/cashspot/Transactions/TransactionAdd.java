package com.dev.tagspot.tgs.cashspot.Transactions;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.DashboardFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Tgs on 3/31/2017.
 */

public class TransactionAdd extends Transaction {

    private static final String TAG = TransactionAdd.class.getSimpleName();
    private Account destinationAccount;
    private float value;
    private String name, category, subcategory, photoURL, destinationAccountName,photoUID, completionFlag;

    private final String type = "Add";
    private int frequency;
    //TODO how to calculate frequency

    public TransactionAdd(TransactionBuilder builder) {
        this.name = builder.name;
        this.value = builder.value;
        this.photoUID = builder.photoUID;
        this.destinationAccount = builder.destinationAccount;
        this.destinationAccountName = builder.destinationAccountName;
        this.category = builder.category;
        this.subcategory = builder.subcategory;
        this.photoURL = builder.photoURL;
        this.date = builder.date;
        this.hour = builder.hour;
        this.minute = builder.minute;
        this.completionFlag = builder.completionFlag;
        Log.d(TAG, "this.name = " + this.name);
        Log.d(TAG, "builder.name = " + builder.name);
        Log.d(TAG, "transactionAdd a = " + toString());
    }

    public TransactionAdd() {
    }

    public TransactionAdd(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public TransactionAdd(Account destination, float value, @Nullable String name, @Nullable String date) {
        this.setValue(value);
        setDestinationAccount(destination);
        if (name != null) {
            this.name = name;
        }
        if (date != null) {
            this.date = date;
        }
    }


    public TransactionAdd(Account destination, float value) {
        synchronized (this) {
            this.setValue(value);
            this.setDestinationAccount(destination);
        }
    }

    public TransactionAdd(Account destination, int value, String name) {
        synchronized (this) {
            this.setValue(value);
            this.setDestinationAccount(destination);
            this.setName(name);
        }

    }

    @Override
    public String toString() {
        return name + "" + String.valueOf(value) + date;

    }

    public String getCompletionFlag() {
        return completionFlag;
    }

    public void setCompletionFlag(String completionFlag) {
        this.completionFlag = completionFlag;
    }

    public String getPhotoUID() {
        return photoUID;
    }

    public void setPhotoUID(String photoUID) {
        this.photoUID = photoUID;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    @Override
    public float getValue() {
        return value;
    }

    @Override
    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getType() {
        return type;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }

    public void executeTransactionAdd(final Context context) {
        final ProgressDialog loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading");
        loadingDialog.show();
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/add");
        String id = reference.push().getKey();

        reference.child(id).setValue(TransactionAdd.this, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    new AlertDialog.Builder(context).setMessage("Error! ").show();
                } else {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    try {
                        ((MainActivity) context).getAccountByName(TransactionAdd.this.getDestinationAccountName()).addToAccount(TransactionAdd.this.getValue());
                        ((MainActivity) context).getAccountByName(TransactionAdd.this.getDestinationAccountName()).updateAccountSumInDB();

                        new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                ((MainActivity) context).changeFragment(new DashboardFragment());
                            }
                        }).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        new AlertDialog.Builder(context).setMessage("Destination account is null! ").show();
                    }

                }
            }
        });

    }

    public void executeTransactionAdd(final Context context, final TransactionAdd add) {
        final ProgressDialog loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading");
        loadingDialog.show();
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/add");
        String id = reference.push().getKey();

        reference.child(id).setValue(add, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    new AlertDialog.Builder(context).setMessage("Error! ").show();
                } else {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    try {
                        ((MainActivity) context).getAccountByName(add.getDestinationAccountName()).addToAccount(add.getValue());
                        ((MainActivity) context).getAccountByName(add.getDestinationAccountName()).updateAccountSumInDB();

                        new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                ((MainActivity) context).changeFragment(new DashboardFragment());
                            }
                        }).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        new AlertDialog.Builder(context).setMessage("Destination account is null! ").show();
                    }

                }
            }
        });

    }



    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void updateTransactionAdd(final Context context, final TransactionAdd add, final CustomSuccessCallback callback){
        Log.d(TAG, "ADD.GETID() = " + add.getId());
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/add");
        ((MainActivity) context).showProgressDialog();
        reference.child(add.getId()).setValue(add, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError == null){

                    ((MainActivity) context).dismissProgressDialog();
                    new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            callback.onSuccess();
                        }
                    }).show();

                } else {
                    ((MainActivity) context).dismissProgressDialog();
                    Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
                    callback.onError();

                }

            }
        });
        //todo rewrite object

    }


    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
        destinationAccount.addToAccount(getValue());
        Log.d(TAG, "setDestinationAccount() + " + getValue());
    }

    public static class TransactionBuilder {
        private Account destinationAccount;
        private float value;
        private String name, category, subcategory, photoURL, date, destinationAccountName, photoUID, completionFlag;
        private int hour, minute;


        public TransactionAdd.TransactionBuilder addFlag(String flag){
            this.completionFlag = flag;
            return this;

        }
        public TransactionBuilder addDestinationAccount(Account destinationAccount) {
            this.destinationAccount = destinationAccount;
            return this;
        }

        public TransactionBuilder addDestinationAccountName(String destinationAccountName) {
            this.destinationAccountName = destinationAccountName;
            return this;
        }

        public TransactionBuilder addValue(float value) {
            Log.e(TAG, "value = " + value);
            this.value = value;
            return this;
        }

        public TransactionBuilder addName(String name) {
            this.name = name;
            Log.d(TAG, "this.name = " + name);
            return this;
        }

        public TransactionBuilder addCategory(String category) {
            this.category = category;
            return this;
        }

        public TransactionBuilder addSubcategory(String subcategory) {
            this.subcategory = subcategory;
            return this;
        }

        public TransactionBuilder addPhotoURL(Uri photoURL) {
            if (photoURL != null) {
                String photo = String.valueOf(photoURL);
                this.photoURL = photo;
            }
            return this;
        }

        public TransactionBuilder addDate(String date) {
            this.date = date;
            return this;
        }

        public TransactionBuilder addPhotoUID(String photoUID) {
            this.photoUID = photoUID;
            return this;
        }
        public TransactionAdd build() {
            return new TransactionAdd(this);
        }

        public TransactionBuilder addHour(int hour) {
            this.hour = hour;
            return this;
        }

        public TransactionBuilder addMinute(int minute) {
            this.minute = minute;
            return this;
        }
    }
}
