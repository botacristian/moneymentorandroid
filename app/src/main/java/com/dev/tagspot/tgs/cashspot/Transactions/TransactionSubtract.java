package com.dev.tagspot.tgs.cashspot.Transactions;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.DashboardFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;

import static com.dev.tagspot.tgs.cashspot.MainActivity.getAccountByName;

/**
 * Created by Tgs on 3/31/2017.
 */

public class TransactionSubtract extends Transaction {

    private static final String TAG = TransactionSubtract.class.getSimpleName();
    private Account sourceAccount;
//    private Account destinationAccount;
    private float value;
//    private String category, subcategory, photoURL,photoUID, completionFlag;
    private String category, subcategory, completionFlag;
    private transient String destinationAccountName;
    transient private final String type = "Subtract";
    //TODO how to calculate frequency

    public TransactionSubtract(TransactionSubtract.TransactionBuilder builder) {
        this.name = builder.name;
        this.value = builder.value;
        this.photoUID = builder.photoUID;
//        this.destinationAccount = builder.destinationAccountBuilder;
        this.destinationAccountName = builder.destinationAccountName;
        this.category = builder.category;
        this.subcategory = builder.subcategory;
        this.photoURL = builder.photoURL;
        this.date = builder.date;
        this.hour = builder.hour;
        this.minute = builder.minute;
        this.completionFlag = builder.completionFlag;
        Log.d(TAG, "this.name = " + this.name);
        Log.d(TAG, "builder.name = " + builder.name);

        Log.d(TAG, "transactionSubtract s = " + toString());
    }

    public String getCompletionFlag() {
        return completionFlag;
    }

    public void setCompletionFlag(String completionFlag) {
        this.completionFlag = completionFlag;
    }

//    @Override
//    public Account getDestinationAccount() {
//        return destinationAccount;
//    }

//    @Override
//    public void setDestinationAccount(Account destinationAccount) {
//        this.destinationAccount = destinationAccount;
//    }

    @Override
    public float getValue() {
        return value;
    }

    @Override
    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    @Override
    public String getPhotoURL() {
        return photoURL;
    }

    @Override
    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    @Override
    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    @Override
    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }

    @Override
    public String getPhotoUID() {
        return photoUID;
    }

    @Override
    public void setPhotoUID(String photoUID) {
        this.photoUID = photoUID;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
//        return new Gson().toJson(this);
        return name + " " + value + " " + date + " "  ;

    }

    public TransactionSubtract() {
    }

    public TransactionSubtract(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public TransactionSubtract(Account source, float value) {
        synchronized (this){
            this.setSourceAccount(source);
            this.setValue(value);
        }
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void executeTransactionSubtract(final Context context) {
        Log.d(TAG, "executeTransactionSubtract");
        final ProgressDialog loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading");
        loadingDialog.show();
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/subtract");
        String id = reference.push().getKey();

        reference.child(id).setValue(TransactionSubtract.this, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    new AlertDialog.Builder(context).setMessage("Error! ").show();
                } else {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    try {
                        getAccountByName(TransactionSubtract.this.getDestinationAccountName()).subtractFromAccount(TransactionSubtract.this.getValue());
                        getAccountByName(TransactionSubtract.this.getDestinationAccountName()).updateAccountSumInDB();

                        new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                ((MainActivity) context).changeFragment(new DashboardFragment());
                            }
                        }).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        new AlertDialog.Builder(context).setMessage("Destination account is null! ").show();
                    }

                }
            }
        });

    }

    public void updateTransactionSubtract(final Context context, final TransactionSubtract subtract, final CustomSuccessCallback callback){
        Log.d(TAG, "SUBTRACT.GETID() = " + subtract.getId());
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/subtract");
        ((MainActivity) context).showProgressDialog();
        reference.child(subtract.getId()).setValue(subtract, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError == null){

                    ((MainActivity) context).dismissProgressDialog();
                    new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            callback.onSuccess();
                        }
                    }).show();

                } else {
                    ((MainActivity) context).dismissProgressDialog();
                    Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
                    callback.onError();

                }

            }
        });
        //todo rewrite object

    }

    public void executeTransactionSubtract(final Context context, final TransactionSubtract subtract) {
        Log.d(TAG, "executeTransactionSubtract");
        final ProgressDialog loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading");
        loadingDialog.show();
//        ((MainActivity) context).showProgressDialog();
        DatabaseReference reference = MainActivity.userIdReference.child("transactions/subtract");
        String id = reference.push().getKey();
        subtract.setId(id);

        reference.child(id).setValue(subtract, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    new AlertDialog.Builder(context).setMessage("Error! ").show();
                } else {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    try {
                        getAccountByName(subtract.getDestinationAccountName()).subtractFromAccount(subtract.getValue());
                        getAccountByName(subtract.getDestinationAccountName()).updateAccountSumInDB();

                        new AlertDialog.Builder(context).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                ((MainActivity) context).changeFragment(new DashboardFragment());
                            }
                        }).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        new AlertDialog.Builder(context).setMessage("Destination account is null! ").show();
                    }

                }
            }
        });

    }

    private void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
        sourceAccount.subtractFromAccount(getValue());
        Log.d(TAG, "setSourceAccount() + " + getValue());
    }

    public static class TransactionBuilder {
        private Account destinationAccountBuilder;
        private float value = 0;
        private String name, category, subcategory, photoURL, date, destinationAccountName, photoUID, completionFlag;
        private int hour, minute;


        public TransactionSubtract.TransactionBuilder addFlag(String flag){
            this.completionFlag = flag;
            return this;

        }

        public TransactionSubtract.TransactionBuilder addDestinationAccount(Account destinationAccount) {
            this.destinationAccountBuilder = destinationAccount;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addDestinationAccountName(String destinationAccountName) {
            this.destinationAccountName = destinationAccountName;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addValue(float value) {
            Log.e(TAG, "value = " + value);
            this.value = value;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addName(String name) {
            this.name = name;
            Log.d(TAG, "this.name = " + name);
            return this;
        }

        public TransactionSubtract.TransactionBuilder addCategory(String category) {
            this.category = category;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addSubcategory(String subcategory) {
            this.subcategory = subcategory;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addPhotoURL(Uri photoURL) {
            if (photoURL != null) {
                String photo = String.valueOf(photoURL);
                this.photoURL = photo;
            }
            return this;
        }

        public TransactionSubtract.TransactionBuilder addDate(String date) {
            this.date = date;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addPhotoUID(String photoUID) {
            this.photoUID = photoUID;
            return this;
        }
        public TransactionSubtract build() {
            return new TransactionSubtract(this);
        }

        public TransactionSubtract.TransactionBuilder addHour(int hour) {
            this.hour = hour;
            return this;
        }

        public TransactionSubtract.TransactionBuilder addMinute(int minute) {
            this.minute = minute;
            return this;
        }
    }
}
