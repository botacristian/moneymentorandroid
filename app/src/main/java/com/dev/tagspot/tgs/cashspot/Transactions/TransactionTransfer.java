package com.dev.tagspot.tgs.cashspot.Transactions;

import com.dev.tagspot.tgs.cashspot.POJO.Account;

/**
 * Created by Tgs on 3/31/2017.
 */

public class TransactionTransfer extends Transaction {

    private TransactionAdd transactionAdd;
    private TransactionSubtract transactionSubtract;

    public TransactionTransfer() {
    }

    public TransactionTransfer(Account source, Account destination, float value){
        transactionSubtract = new TransactionSubtract(source, value);
        transactionAdd = new TransactionAdd(destination, value);
    }

}
