package com.dev.tagspot.tgs.cashspot.Transactions;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.POJO.Transfer;
import com.dev.tagspot.tgs.cashspot.R;

import java.util.ArrayList;

/**
 * Created by Tgs on 5/4/2017.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    private ArrayList<Transaction> transactionList;

    public TransactionsAdapter(ArrayList<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item_view, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindStuff(position);
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name, amount, to, from, staticFrom, type;

        ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.nameTv);
            amount = (TextView) itemView.findViewById(R.id.amountTv);
            to = (TextView) itemView.findViewById(R.id.toTv);
            from = (TextView) itemView.findViewById(R.id.fromTv);
            staticFrom = (TextView) itemView.findViewById(R.id.fromStaticTv);
            type = (TextView) itemView.findViewById(R.id.typeTv);
        }

        void bindStuff(int position) {
            //TODO if some fields are null or empty hide the static Text before them
            Transaction t = transactionList.get(position);
            name.setText(t.getName());
            amount.setText(String.valueOf(t.getValue()));
            if(t.getType().equals(Transaction.TRANSFER)){
                from.setVisibility(View.VISIBLE);
                staticFrom.setVisibility(View.VISIBLE);
                //TODO Set origin account
//                t.getOriginAccount();
              } else {
                from.setVisibility(View.GONE);
                staticFrom.setVisibility(View.GONE);
            }
            if (t.getDestinationAccountName() != null) {
                to.setText(t.getDestinationAccountName());
            }
            if (t.getType() != null) {
                type.setText(t.getType());
            }
        }
    }
}
