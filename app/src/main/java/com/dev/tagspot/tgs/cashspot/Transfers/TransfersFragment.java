package com.dev.tagspot.tgs.cashspot.Transfers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dev.tagspot.tgs.cashspot.DashboardFragment;
import com.dev.tagspot.tgs.cashspot.MainActivity;
import com.dev.tagspot.tgs.cashspot.POJO.Account;
import com.dev.tagspot.tgs.cashspot.POJO.Transfer;
import com.dev.tagspot.tgs.cashspot.R;
import com.dev.tagspot.tgs.cashspot.Utils.CustomSuccessCallback;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.List;

import static com.dev.tagspot.tgs.cashspot.MainActivity.getAccountByName;

/**
 * Created by Tgs on 5/2/2017.
 */

public class TransfersFragment extends Fragment implements View.OnClickListener, CustomSuccessCallback {

    private  static final String TAG = TransfersFragment.class.getSimpleName();
    public float amount;
    EditText amountEt;
    MaterialSpinner originAccount, destinationAccount;
    public String originAccountName, destinationAccountName;
    Button submit;
    List<String> accountNames;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.transfer_fragment, container, false);
        accountNames = ((MainActivity) getActivity()).getAccountsNames();
        amountEt = (EditText) v.findViewById(R.id.amountEt);
        originAccount = (MaterialSpinner) v.findViewById(R.id.originAccount);
        try {
            originAccount.setItems(accountNames);
        } catch (Exception e){
            e.printStackTrace();
        }
        originAccount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                originAccountName = item.toString();
            }
        });
        originAccountName = accountNames.get(originAccount.getSelectedIndex());
//        originAccount.setSelectedIndex(0);
        destinationAccount = (MaterialSpinner) v.findViewById(R.id.destinationAccount);
        destinationAccount.setItems(accountNames);
        destinationAccount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                destinationAccountName = item.toString();
            }
        });

        destinationAccountName = accountNames.get(destinationAccount.getSelectedIndex());
//        originAccount.setItems(((MainActivity) getActivity()).getAccountList());
//        destinationAccount.setItems(MainActivity.accountLis);

        submit = (Button) v.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                getValue();
                break;
        }
    }

    private void getValue() {
        try{
            amount = Float.valueOf(amountEt.getText().toString());
            if(amount != 0){
                checkAccounts();
            } else {
                new AlertDialog.Builder(getActivity()).setMessage("Invalid amount").show();
            }
        } catch (Exception e){
            e.printStackTrace();

        }
    }

    private void checkAccounts() {
        if (originAccountName.equals(destinationAccountName)) {
            new AlertDialog.Builder(getActivity()).setMessage("Can not transfer to the same account").show();
        } else {
            //Todo transfer(originAccount, destinationAccount, amount)
            Transfer t = new Transfer(amount, originAccountName, destinationAccountName);
            Log.d(TAG, "transfer T = "+ amount + originAccountName + " " + destinationAccountName);
            executeTransfer(t);

        }
    }

    private void executeTransfer(Transfer t){
        final ProgressDialog loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setMessage("Loading");
        loadingDialog.show();

        DatabaseReference reference = MainActivity.userIdReference.child("transactions/transfers");
        String id = reference.push().getKey();

        reference.child(id).setValue(t, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError != null){
                    if(loadingDialog.isShowing()){
                        loadingDialog.dismiss();
                    }
                    new AlertDialog.Builder(getActivity()).setMessage("Error! ").show();
                } else {
                    if(loadingDialog.isShowing()){
                        loadingDialog.dismiss();
                    }
                    //TODO Update both accounts
                    getAccountByName(originAccountName).subtractFromAccountWithReturn(amount).updateAccountSumInDB();
                    getAccountByName(destinationAccountName).addToAccountWithReturn(amount).updateAccountSumInDB(TransfersFragment.this);
                    //TODO UPDATE

                    //cauta accountul dupa nume din lista
                    //scade sau aduna amountul
                    //update in DB

                    //idem
                }
            }
        });
    }

    @Override
    public void onSuccess() {
        new AlertDialog.Builder(getActivity()).setMessage("Success!").setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                ((MainActivity) getActivity()).changeFragment(new DashboardFragment());
            }
        }).show();

    }

    @Override
    public void onError() {
        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
    }
}
