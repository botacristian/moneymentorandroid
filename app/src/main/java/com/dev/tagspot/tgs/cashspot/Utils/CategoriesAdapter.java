package com.dev.tagspot.tgs.cashspot.Utils;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.OnItemClicked;
import com.dev.tagspot.tgs.cashspot.OnItemLongTouch;
import com.dev.tagspot.tgs.cashspot.POJO.Category;
import com.dev.tagspot.tgs.cashspot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristi on 5/7/2017.
 */

public class CategoriesAdapter extends BaseAdapter {
    private static final String TAG = CategoriesAdapter.class.getSimpleName();
    private List<Category> categories = new ArrayList<>();
    private OnItemClicked onItemClicked;
    private OnItemLongTouch onItemLongTouch;
    private Category category;
    Context c;
    private int selectedPosition = -1;

    public CategoriesAdapter(Context c) {
        this.c = c;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    public void setOnItemLongTouch(OnItemLongTouch onItemLongTouch) {
        this.onItemLongTouch = onItemLongTouch;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }


    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        if (categories != null) {
            Log.d("CategoriesAdapter", " returning getItem = " + categories.get(position).getName());
            return categories.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_item, parent, false);
        }
        category = categories.get(position);


        final ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        final TextView textView = (TextView) convertView.findViewById(R.id.titleTv);

        if(position == selectedPosition){
            convertView.setBackgroundColor(Color.GREEN);
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                category = categories.get(position);
                if (category.getParentId() == -1 ) {
                    if(onItemClicked != null){
                    onItemClicked.onCategoryClicked(category);
                    selectedPosition = position;
                    notifyDataSetChanged();


                    }
                }
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                category = categories.get(position);
                if (category.isDeletable()) {
                    onItemLongTouch.onCategoryLongTouch(category);

                }
                return true;
            }
        });

        try {
            textView.setText(category.getName());
            imageView.setImageDrawable(category.getIconDrawable(c));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private void setSelectedPosition(int position){
        selectedPosition = position;
    }

    public void swapCategories(ArrayList<Category> subcategories) {
        Log.d(TAG, "swapCategories");
        this.categories = subcategories;
        notifyDataSetChanged();
    }


}

