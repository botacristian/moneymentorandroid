package com.dev.tagspot.tgs.cashspot.Utils;

import android.app.Activity;
import android.content.Intent;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 4/18/2017.
 */

public class CustomActivity extends Activity {

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }



}
