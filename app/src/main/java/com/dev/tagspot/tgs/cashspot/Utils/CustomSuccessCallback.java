package com.dev.tagspot.tgs.cashspot.Utils;

/**
 * Created by Tgs on 9/13/2017.
 */

public interface CustomSuccessCallback {

    void onSuccess();

    void onError();
}
