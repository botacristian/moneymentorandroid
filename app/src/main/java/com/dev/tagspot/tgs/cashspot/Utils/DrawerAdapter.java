package com.dev.tagspot.tgs.cashspot.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.tagspot.tgs.cashspot.R;

/**
 * Created by Tgs on 5/2/2017.
 */

public class DrawerAdapter extends BaseAdapter {
    private Context context;
    private String[] list;
    private int[] images;
    private LayoutInflater inflater;
    ViewHolder holder;

    public DrawerAdapter() {
    }

    public DrawerAdapter(Context context, String[] list, int[] images){
        this.context = context;
        this.list = list;
        this.images = images;
        inflater = LayoutInflater.from(this.context);
    }
    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int position) {
        return list[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
            holder.textView = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(getItem(position).toString());
        holder.imageView.setImageResource(images[position]);
        return convertView;
    }

    class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}
